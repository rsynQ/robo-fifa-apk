import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/avatar/gf_avatar.dart';
import 'package:getwidget/components/drawer/gf_drawer.dart';
import 'package:getwidget/components/drawer/gf_drawer_header.dart';

import '../../routes/pages_route.dart';
import '../auth/controller/auth_controller.dart';
import '../helpers/launch_helper.dart';
import '../ui/app_colors.dart';
import '../ui/terms/terms_of_service_dialog.dart';
import 'item_menu.dart';

class DrawerGf extends StatelessWidget {
  const DrawerGf({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(builder: (controller) {
      return GFDrawer(
        elevation: 2,
        color: AppColors.primaryColor,
        child: Column(
          children: [
            GFDrawerHeader(
              decoration: const BoxDecoration(
                color: Color(0xFFA0A8A8),
              ),
              closeButton: const SizedBox.shrink(),
              otherAccountsPictures: const [
                GFAvatar(
                  backgroundImage: AssetImage('assets/logo/icon.png'),
                )
              ],
              child: Visibility(
                visible: controller.isLogged(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${controller.user?.username}',
                      style: const TextStyle(
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    ),
                    Row(
                      children: [
                        const Icon(Icons.mail),
                        const SizedBox(width: 5),
                        Text(
                          '${controller.user?.email}',
                          style: const TextStyle(
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.italic,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: controller.isLogged(),
              child: ItemMenu(
                label: 'SAIR AGORA',
                icon: Icons.power_settings_new_sharp,
                onTap: () async {
                  bool confirmLogout = await Get.dialog(
                    AlertDialog(
                      alignment: Alignment.center,
                      backgroundColor: AppColors.primaryColor,
                      title: Text('CONFIRMAÇÃO',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.textColor,
                              fontFamily: 'Roboto',
                              fontSize: 16,
                              fontWeight: FontWeight.bold)),
                      content: Text('Tem certeza de que deseja sair agora?',
                          style: TextStyle(
                            color: AppColors.secundaryColor,
                            fontFamily: 'Roboto',
                            fontSize: 14,
                          )),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Get.back(result: false),
                          child: Text('CANCELAR',
                              style: TextStyle(
                                color: AppColors.secundaryColor,
                                fontFamily: 'Roboto',
                                fontSize: 12,
                              )),
                        ),
                        TextButton(
                          onPressed: () => Get.back(result: true),
                          child: Text('SIM, DESEJO SAIR',
                              style: TextStyle(
                                color: AppColors.textColor,
                                fontFamily: 'Roboto',
                                fontSize: 12,
                              )),
                        ),
                      ],
                    ),
                  );

                  if (confirmLogout == true) {
                    await controller.logout();
                  }
                },
              ),
            ),
            Visibility(
              visible: controller.isLogged(),
              child: ExpansionTile(
                leading: Icon(Icons.person, color: AppColors.textColor),
                iconColor: AppColors.textColor,
                collapsedIconColor: AppColors.textColor,
                title: const Text(
                  'MEUS DADOS',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                children: [
                  ItemMenu(
                      label: 'TERMO DE USO',
                      icon: Icons.note,
                      onTap: () => Get.dialog(const TermsOfServiceDialog())),
                  // const ItemMenu(label: 'TROCAR SENHA', icon: Icons.password),
                  ItemMenu(
                      label: 'EXCLUIR MINHA CONTA',
                      icon: Icons.delete,
                      onTap: () async {
                        final result = await Get.dialog(
                          AlertDialog(
                            content: const Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  'Você tem certeza de que deseja excluir sua conta permanentemente?',
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(height: 20),
                                Text(
                                  'Ao prosseguir com esta ação, todos os dados associados à sua conta serão removidos de forma irreversível. Certifique-se de fazer backup de qualquer informação importante antes de continuar.',
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.black),
                                ),
                                SizedBox(height: 20),
                                Text(
                                  'Por favor, note que esta ação pode levar algum tempo para ser processada completamente.',
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.black),
                                ),
                              ],
                            ),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  // Implemente a lógica para cancelar a exclusão da conta
                                  Get.back();
                                },
                                child: const Text(
                                  'Cancelar',
                                  style: TextStyle(color: Colors.blue),
                                ),
                              ),
                              TextButton(
                                onPressed: () async {
                                  Get.back();
                                  await controller.logout();
                                },
                                child: const Text(
                                  'Excluir Conta',
                                  style: TextStyle(color: Colors.red),
                                ),
                              ),
                            ],
                          ),
                        );

                        if (result) {
                          Get.back();
                        }
                      }),
                ],
              ),
            ),
            ItemMenu(
                label: 'MEUS BOTS',
                icon: Icons.bolt,
                onTap: () {
                  if (controller.isLogged()) {
                    Get.toNamed(PagesRoute.botRoute);
                  } else {
                    Get.back();
                    Get.toNamed(PagesRoute.signInRoute);
                  }
                }),
            ItemMenu(
                label: 'ALERTAS',
                icon: Icons.message,
                onTap: () {
                  if (controller.isLogged()) {
                    Get.toNamed(PagesRoute.alertRoute);
                  } else {
                    Get.back();
                    Get.toNamed(PagesRoute.signInRoute);
                  }
                }),
            ItemMenu(
                label: 'PLANOS',
                icon: Icons.monetization_on,
                onTap: () => Get.toNamed(PagesRoute.plans)),
            ItemMenu(
              label: 'ROBÔ BET (FUTEBOL)',
              icon: Icons.download,
              onTap: () async => await LaunchHelper.getLaunchUrl(Uri.parse(Platform
                      .isAndroid
                  ? 'https://play.google.com/store/apps/details?id=br.com.robobet'
                  : 'https://apps.apple.com/br/app/robobet/id6478976410')),
            )
          ],
        ),
      );
    });
  }
}
