import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/fcm/fcm_service.dart';

import 'notification_service.dart';

class FcmController extends GetxController {
  late FcmService _service;
  final notification = NotificationService();

  @override
  void onInit() async {
    await FirebaseMessaging.instance.setAutoInitEnabled(true);
    _service = FcmService(notification);
    _service.initialize();
    super.onInit();
  }
}
