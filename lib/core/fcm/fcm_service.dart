import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

import '../../models/notification/custom_notification.dart';
import 'notification_service.dart';

class FcmService {
  final NotificationService _notificationService;

  FcmService(this._notificationService);

  Future<void> initialize() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    _onMessage();
    _onMessageOpenedApp();
  }

  Future<String?> getDeviceToken() => FirebaseMessaging.instance.getToken();

  _onMessage() {
    FirebaseMessaging.onMessage.listen((message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;

      if (notification != null && android != null) {
        _notificationService.showNotification(CustomNotification(
          id: android.hashCode,
          title: notification.title,
          body: notification.body,
          payload: message.data['route'] ?? '/',
        ));
      }
    });
  }

  _onMessageOpenedApp() {
    FirebaseMessaging.onMessageOpenedApp.listen(_goToPageAfterMessage);
  }

  _goToPageAfterMessage(message) {
    final String route = message.data['route'] ?? '';
    if (route.isNotEmpty && route != '/') {
      Get.toNamed(route);
    }
  }
}
