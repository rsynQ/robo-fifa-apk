import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone_updated_gradle/flutter_native_timezone.dart';
import 'package:get/get.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest_all.dart' as tz;

import '../../models/notification/custom_notification.dart';
import '../ui/app_colors.dart';

class NotificationService {
  late FlutterLocalNotificationsPlugin localNotificationsPlugin;
  late AndroidNotificationDetails androidDetails;

  NotificationService() {
    localNotificationsPlugin = FlutterLocalNotificationsPlugin();
    _setupNotifications();
  }

  _setupNotifications() async {
    await _setupTimeZone();
    await _initializeNotifications();
  }

  _setupTimeZone() async {
    tz.initializeTimeZones();
    final String timeZoneName = await FlutterNativeTimezone.getLocalTimezone();
    tz.setLocalLocation(tz.getLocation(timeZoneName));
  }

  _initializeNotifications() async {
    const android = AndroidInitializationSettings('@mipmap/ic_launcher');

    await localNotificationsPlugin.initialize(
      const InitializationSettings(android: android),
      onDidReceiveNotificationResponse: (details) {
        if (details.payload != null) {}
        Get.toNamed(details.payload!);
      },
    );
  }

  _onSelectNotification(String? payload) {
    if (payload != null && payload.isNotEmpty) {}
  }

  showNotification(CustomNotification notification) {
    androidDetails = AndroidNotificationDetails('rfifa_notification', 'rfifa',
        channelDescription:
            'Este canal é para receber as principais notificações',
        importance: Importance.max,
        priority: Priority.max,
        enableVibration: true,
        color: AppColors.primaryColor,
        playSound: true,
        sound: const RawResourceAndroidNotificationSound('notification_sound'),
        actions: []);

    localNotificationsPlugin.show(notification.id, notification.title,
        notification.body, NotificationDetails(android: androidDetails),
        payload: notification.payload);
  }

  showNotificationSchedule(
      CustomNotification notification, String startTime, int timer) {
    DateTime startDateTime = DateTime.parse(startTime);
    DateTime notificationTime =
        startDateTime.subtract(Duration(minutes: timer));

    androidDetails = AndroidNotificationDetails('rfifa_notification', 'rfifa',
        channelDescription:
            'Este canal é para receber as principais notificações',
        importance: Importance.max,
        priority: Priority.max,
        enableVibration: true,
        color: AppColors.primaryColor,
        playSound: true,
        sound: const RawResourceAndroidNotificationSound('notification_sound'));

    localNotificationsPlugin.zonedSchedule(
      notification.id,
      notification.title,
      notification.body,
      tz.TZDateTime.from(notificationTime, tz.local),
      NotificationDetails(android: androidDetails),
      payload: notification.payload,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  Future<bool> checkIfNotificationScheduled(int notificationId) async {
    List<PendingNotificationRequest> pendingNotifications =
        await localNotificationsPlugin.pendingNotificationRequests();

    bool isScheduled = pendingNotifications
        .any((notification) => notification.id == notificationId);

    return isScheduled;
  }

  Future<void> cancelScheduledNotification(int notificationId) async {
    await localNotificationsPlugin.cancel(notificationId);
  }

  checkForNotifications() async {
    final details =
        await localNotificationsPlugin.getNotificationAppLaunchDetails();
    if (details != null && details.didNotificationLaunchApp) {
      _onSelectNotification(details.notificationResponse?.payload);
    }
  }
}
