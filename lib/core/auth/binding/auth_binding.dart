import 'package:get/get.dart';
import 'package:robo_fifa/core/fcm/fcm_controller.dart';

import '../controller/auth_controller.dart';

class AuthBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(FcmController());
    Get.put(AuthController());
  }
}
