// ignore_for_file: depend_on_referenced_packages

import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../models/token/token_model.dart';

part 'token_result.freezed.dart';

@freezed
class TokenResult with _$TokenResult {
  factory TokenResult.success(TokenModel data) = Success;
  factory TokenResult.error(String message) = Error;
}
