// ignore_for_file: depend_on_referenced_packages

import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../models/user/user_model.dart';

part 'user_result.freezed.dart';

@freezed
class UserResult with _$UserResult {
  factory UserResult.success(UserModel data) = Success;
  factory UserResult.error(String message) = Error;
}
