import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../models/match/match_model.dart';
import '../../../models/player/player_model.dart';
import '../../../models/ranking/ranking_model.dart';
import '../../../models/token/token_model.dart';
import '../../../models/user/user_model.dart';
import '../../../routes/pages_route.dart';
import '../../constants/storage_keys.dart';
import '../../helpers/date_helper.dart';
import '../../helpers/ranking_helper.dart';
import '../../helpers/secure_storage.dart';
import '../repository/auth_repository.dart';
import '../result/token_result.dart';
import '../result/user_result.dart';

class AuthController extends GetxController {
  final repository = AuthRepository();
  final RxList<MatchModel> _matches = RxList([]);
  final RxList<MatchModel> _matchesClosed = RxList([]);
  final RxList<MatchModel> _matchesOpen = RxList([]);
  final RxList<RankingModel> _rankings = RxList([]);
  final RxList<PlayerModel> _players = RxList([]);

  List<MatchModel> get matches => _matches;
  List<MatchModel> get matchesClosed => _matchesClosed;
  List<MatchModel> get matchesOpen => _matchesOpen;
  List<RankingModel> get rankings => _rankings;
  List<PlayerModel> get players => _players;

  Map<int, List<MatchModel>> groupedMatches = {};
  UserModel? user;

  @override
  void onInit() async {
    await getMatches();
    await getPlayers();
    await refreshToken();
    await Get.offAllNamed(PagesRoute.welcomeRoute);
    super.onInit();
  }

  Future<void> login(String email, String password) async {
    EasyLoading.show(status: 'Verificando suas informações.');
    final result = await repository.login(email, password);
    result.when(success: (tokenModel) async {
      await confirm(accessToken: tokenModel.access_token);
    }, error: (message) {
      EasyLoading.showError(message);
    });
  }

  Future<void> createAccount(UserModel userModel) async {
    EasyLoading.show(status: 'Verificando suas informações.');
    final result = await repository.createAccount(userModel);
    result.when(success: (tokenModel) async {
      await confirm(accessToken: tokenModel.access_token);
    }, error: (message) {
      EasyLoading.showError(message);
    });
  }

  Future<void> logout() async {
    EasyLoading.show(status: 'Encerrando sessão...');
    await repository.logout();
    EasyLoading.dismiss();
    user = null;
    update();
    Get.offAllNamed(PagesRoute.welcomeRoute);
  }

  Future<void> confirm({required String accessToken}) async {
    EasyLoading.show(status: 'Validando seu acesso.');
    String? androidToken = await FirebaseMessaging.instance.getToken() ?? '';

    bool hasNotificationPermission = await checkNotificationPermission();

    if (!hasNotificationPermission) {
      await Permission.notification.request();
    }

    TokenResult result = await repository.confirmToken(
        acessToken: accessToken, androidToken: androidToken);

    result.when(
      success: (tokenModel) async {
        await saveToken(tokenModel);
        await Future.delayed(const Duration(seconds: 1));
        await me();
        Get.offAllNamed(PagesRoute.welcomeRoute);
      },
      error: (message) => EasyLoading.showError(message),
    );
  }

  Future<void> refreshToken() async {
    String? token = await FirebaseMessaging.instance.getToken() ?? '';
    String? refreshToken =
        await SecureStorage.getLocalData(key: StorageKeys.refreshToken);

    if (refreshToken == null) {
      await SecureStorage.removeLocalData(key: StorageKeys.accessToken);
      await SecureStorage.removeLocalData(key: StorageKeys.refreshToken);
      Get.offAllNamed(PagesRoute.welcomeRoute);
      return;
    }

    TokenResult result = await repository.validateToken(refreshToken, token);

    result.when(success: (data) async {
      await saveToken(data);
      await Future.delayed(const Duration(milliseconds: 500));
    }, error: (message) {
      EasyLoading.showError(message);
      Get.offAllNamed(PagesRoute.welcomeRoute);
    });
  }

  Future<void> me() async {
    EasyLoading.show(status: 'Estamos finalizando...');

    UserResult result = await repository.me();

    result.when(
      success: (data) async {
        user = data;
        EasyLoading.dismiss();
        update();
      },
      error: (message) {
        EasyLoading.showError(message);
      },
    );
  }

  Future<void> saveToken(TokenModel tokenModel) async {
    SecureStorage.removeLocalData(key: StorageKeys.accessToken);
    SecureStorage.removeLocalData(key: StorageKeys.refreshToken);
    SecureStorage.saveLocalData(
        key: StorageKeys.accessToken, data: tokenModel.access_token);
    SecureStorage.saveLocalData(
        key: StorageKeys.refreshToken, data: (tokenModel.refresh_token!));
    await Future.delayed(const Duration(seconds: 1));
  }

  Future<void> getMatches() async {
    final response = await repository.getReports();

    response.when(
        success: (data) async {
          _matches.assignAll(data);
          _matchesClosed
              .assignAll(_matches.where((p0) => p0.finished == 1).toList());
          _matchesOpen
              .assignAll(_matches.where((p0) => p0.finished == 0).toList());
          treatData(false);
        },
        error: (message) {});
  }

  Future<void> getPlayers() async {
    final response = await repository.getPlayers();

    response.when(
        success: (data) async {
          _players.assignAll(data);
        },
        error: (message) {});
  }

  Future<bool> checkNotificationPermission() async {
    var status = await Permission.notification.status;
    return status.isGranted;
  }

  void treatData(bool isHours) {
    groupedMatches.clear();
    rankings.clear();

    final now = DateTime.now();
    final twoHoursAgo = now.subtract(const Duration(hours: 2));
    final eightHoursAgo = now.subtract(const Duration(hours: 4));

    final matchesToConsider = isHours
        ? matchesClosed.where((match) {
            final data = DateHelper.convertTimestampToDateTime(
                int.parse(match.datetime));
            return data.isAfter(twoHoursAgo);
          }).toList()
        : matchesClosed.where((match) {
            final data = DateHelper.convertTimestampToDateTime(
                int.parse(match.datetime));
            return data.isAfter(eightHoursAgo);
          }).toList();

    __groupMatchesByPlayers(matchesToConsider).forEach((playerId, matches) {
      final name = RankingHelper.playerName(matches, playerId);
      final j = matches.length;
      final v = RankingHelper.countPlayerWins(matches, playerId);
      final d = RankingHelper.countPlayerLoss(matches, playerId);
      final e = (j - (v + d));
      final gp = RankingHelper.countPlayerGoals(matches, playerId);
      final mgp = (j / gp).ceilToDouble();
      final gc = RankingHelper.countPlayerGoalsConceded(matches, playerId);
      final mgc = (j / gc).ceilToDouble();
      final sg = (gp - gc);
      final p = ((v * 3) + (e * 1));
      final ap = ((p / (j * 3)) * 100).ceilToDouble();
      final g05ht =
          RankingHelper.countMatchesGoalHtByPlayer(matches, playerId, 0.5);
      final g15ht =
          RankingHelper.countMatchesGoalHtByPlayer(matches, playerId, 1.5);
      final g25ht =
          RankingHelper.countMatchesGoalHtByPlayer(matches, playerId, 2.5);
      final g35ht =
          RankingHelper.countMatchesGoalHtByPlayer(matches, playerId, 3.5);
      final g05 =
          RankingHelper.countMatchesGoalByPlayer(matches, playerId, 0.5);
      final g15 =
          RankingHelper.countMatchesGoalByPlayer(matches, playerId, 1.5);
      final g25 =
          RankingHelper.countMatchesGoalByPlayer(matches, playerId, 2.5);
      final g35 =
          RankingHelper.countMatchesGoalByPlayer(matches, playerId, 3.5);
      final g45 =
          RankingHelper.countMatchesGoalByPlayer(matches, playerId, 4.5);
      final btsHt = RankingHelper.countMatchesBttsHt(matches);
      final btsFt = RankingHelper.countMatchesBtts(matches);

      _rankings.add(RankingModel(
          name: name,
          playerID: playerId,
          j: j,
          v: v,
          d: d,
          e: e,
          gp: gp,
          mgp: mgp,
          gc: gc,
          mgc: mgc,
          sg: sg,
          p: p,
          ap: ap,
          g05: g05,
          g15: g15,
          g25: g25,
          g35: g35,
          g45: g45,
          g05ht: g05ht,
          g15ht: g15ht,
          g25ht: g25ht,
          g35ht: g35ht,
          bts_ht: btsHt,
          bts_ft: btsFt));
    });
  }

  bool isLogged() {
    return user?.id != null ? true : false;
  }

  bool isPRO() {
    return (user?.pro ?? 0) > 0 ? true : false;
  }

  Map<int, List<MatchModel>> __groupMatchesByPlayers(List<MatchModel> matches) {
    for (var match in matches) {
      groupedMatches.putIfAbsent(match.home_player_id, () => []);
      groupedMatches.putIfAbsent(match.away_player_id, () => []);
      groupedMatches[match.home_player_id]?.add(match);
      groupedMatches[match.away_player_id]?.add(match);
    }

    return groupedMatches;
  }
}
