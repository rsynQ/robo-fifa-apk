import 'package:robo_fifa/models/user/user_model.dart';

import '../../../models/match/match_model.dart';
import '../../../models/player/player_model.dart';
import '../../../models/token/token_model.dart';
import '../../constants/endpoints.dart';
import '../../constants/storage_keys.dart';
import '../../helpers/secure_storage.dart';
import '../../http/http_manager.dart';
import '../../result/data_result.dart';
import '../result/token_result.dart';
import '../result/user_result.dart';

class AuthRepository {
  final HttpManager _http = HttpManager();

  /* Get Players*/
  Future<DataResult<PlayerModel>> getPlayers() async {
    final response = await _http.restRequest(
      url: Endpoints.players,
      method: HttpMethods.get,
    );

    if (response['result'] != null) {
      List<PlayerModel> data =
          (List<Map<String, dynamic>>.from(response['result']))
              .map(PlayerModel.fromMap)
              .toList();
      return DataResult.success(data);
    } else {
      return DataResult.error(response['error']);
    }
  }

  /* Get Reports*/
  Future<DataResult<MatchModel>> getReports() async {
    final response = await _http.restRequest(
      url: Endpoints.matches,
      method: HttpMethods.get,
    );

    if (response['result'] != null) {
      List<MatchModel> data =
          (List<Map<String, dynamic>>.from(response['result']))
              .map(MatchModel.fromMap)
              .toList();
      return DataResult.success(data);
    } else {
      return DataResult.error(response['error']);
    }
  }

  /* Login */
  Future<TokenResult> login(String email, String password) async {
    final result = await _http.restRequest(
        url: Endpoints.login,
        method: HttpMethods.post,
        body: {'email': email, 'password': password});

    return handleAcessTokenOrError(result);
  }

  /* Create Account */
  Future<TokenResult> createAccount(UserModel user) async {
    final result = await _http.restRequest(
        url: Endpoints.create, method: HttpMethods.post, body: user.toMap());

    return handleAcessTokenOrError(result);
  }

  /* Logout */
  Future<void> logout() async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    await _http.restRequest(
        url: Endpoints.logout,
        method: HttpMethods.post,
        headers: {'Authorization': 'Bearer $token'});
    await SecureStorage.removeLocalData(key: StorageKeys.accessToken);
    await SecureStorage.removeLocalData(key: StorageKeys.refreshToken);
  }

  Future<TokenResult> confirmToken(
      {required String acessToken, required String androidToken}) async {
    final result = await _http.restRequest(
        url: Endpoints.confirm,
        method: HttpMethods.post,
        headers: {'Authorization': 'Bearer $acessToken'},
        body: {'token': androidToken});

    return handleAcessTokenOrError(result);
  }

  Future<UserResult> me() async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    final result = await _http.restRequest(
        url: Endpoints.me,
        method: HttpMethods.get,
        headers: {'Authorization': 'Bearer $token'});

    if (result['result'] != null) {
      final data = UserModel.fromMap(result['result']);
      return UserResult.success(data);
    } else {
      return UserResult.error(result['error']);
    }
  }

  Future<TokenResult> validateToken(
      String refreshToken, String androidToken) async {
    final result = await _http.restRequest(
        url: Endpoints.refresh,
        method: HttpMethods.post,
        body: {'refresh_token': refreshToken, 'token': androidToken});

    return handleAcessTokenOrError(result);
  }

  TokenResult handleAcessTokenOrError(Map<dynamic, dynamic> result) {
    if (result['result'] != null) {
      final accessTokenModel = TokenModel.fromMap(result['result']);
      return TokenResult.success(accessTokenModel);
    } else {
      return TokenResult.error(result['error']);
    }
  }
}
