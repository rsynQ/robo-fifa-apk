// ignore_for_file: deprecated_member_use

import 'package:dio/dio.dart';

import 'interceptors/auth_interceptor.dart';
import 'interceptors/connectivy_interceptor.dart';

abstract class HttpMethods {
  static const String post = 'POST';
  static const String get = 'GET';
  static const String put = 'PUT';
  static const String patch = 'PATCH';
  static const String delete = 'DELETE';
}

class HttpManager {
  Future<Map<String, dynamic>> restRequest(
      {required String url,
      required String method,
      Map? headers,
      Map? body,
      Object? file}) async {
    // Headers da requisição
    final defaultHeaders = headers?.cast<String, String>() ?? {}
      ..addAll({
        'content-type': 'application/json',
        'accept': 'application/json',
      });

    Dio dio = Dio()
      ..interceptors.addAll([
        AuthInterceptor(),
        ConnectivyInterceptor(),
      ])
      ..options.connectTimeout = const Duration(milliseconds: 15000);

    try {
      Response response = await dio.request(
        url,
        options: Options(
          headers: defaultHeaders,
          method: method,
        ),
        data: file ?? body,
      );

      return response.data;
    } on DioError catch (error) {
      return error.response?.data ?? {};
    } catch (error) {
      return {};
    }
  }
}
