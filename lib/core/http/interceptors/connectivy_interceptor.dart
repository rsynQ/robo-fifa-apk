import 'package:dio/dio.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../../../routes/pages_route.dart';

class ConnectivyInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (await isConnected() != true) {
      EasyLoading.showInfo('Você está sem conexão com a internet!');
      Get.toNamed(PagesRoute.noInternetRoute);
    }

    return handler.next(options);
  }

  Future<bool> isConnected() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    return connectivityResult != ConnectivityResult.none;
  }
}
