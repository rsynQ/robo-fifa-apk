// ignore_for_file: deprecated_member_use

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart' as getConnect;
import 'package:robo_fifa/core/auth/result/token_result.dart';
import 'package:robo_fifa/models/token/token_model.dart';

import '../../../routes/pages_route.dart';
import '../../auth/repository/auth_repository.dart';
import '../../constants/storage_keys.dart';
import '../../helpers/secure_storage.dart';

class AuthInterceptor extends Interceptor {
  int maxRetryAttempts = 3;
  TokenModel? accessTokenModel;
  final AuthRepository _auth = AuthRepository();

  @override
  onError(DioError err, ErrorInterceptorHandler handler) async {
    final statusCode = err.response?.statusCode;

    String? accessToken =
        await SecureStorage.getLocalData(key: StorageKeys.accessToken);

    String? refreshToken =
        await SecureStorage.getLocalData(key: StorageKeys.refreshToken);

    if ((statusCode == 403 || statusCode == 401) &&
        accessToken != null &&
        refreshToken != null) {
      final retryAttempts = await _getRetryAttempts();

      if (retryAttempts >= maxRetryAttempts) {
        SecureStorage.removeLocalData(key: StorageKeys.accessToken);
        SecureStorage.removeLocalData(key: StorageKeys.refreshToken);
        getConnect.Get.offAllNamed(PagesRoute.welcomeRoute);
        await _resetRetryAttempts();
        return;
      }

      await _incrementRetryAttempts();
      await _refreshToken();
      return _retryRequest(err, handler);
    }

    return handler.next(err);
  }

  Future<void> _refreshToken() async {
    try {
      String? accessToken =
          await SecureStorage.getLocalData(key: StorageKeys.accessToken);

      String? refreshToken =
          await SecureStorage.getLocalData(key: StorageKeys.refreshToken);

      if (accessToken == null || refreshToken == null) {
        getConnect.Get.offAllNamed(PagesRoute.welcomeRoute);
        return;
      }

      String? androidToken = await FirebaseMessaging.instance.getToken() ?? '';

      TokenResult result =
          await _auth.validateToken(refreshToken, androidToken);

      result.when(
        success: (accessTokenModel) async {
          this.accessTokenModel = accessTokenModel;
          await _resetRetryAttempts();
          __saveTokenAndProceedToBase();
        },
        error: (message) {},
      );
      // ignore: empty_catches
    } on Exception {}
  }

  Future<int> _getRetryAttempts() async {
    final result =
        await SecureStorage.getLocalData(key: StorageKeys.retryAttempts) ?? '0';
    return int.parse(result);
  }

  Future<void> _incrementRetryAttempts() async {
    final retryAttempts = await _getRetryAttempts();
    final result = retryAttempts + 1;
    await SecureStorage.saveLocalData(
        key: StorageKeys.retryAttempts, data: result.toString());
  }

  Future<void> _resetRetryAttempts() async {
    await SecureStorage.removeLocalData(key: StorageKeys.retryAttempts);
  }

  Future<void> _retryRequest(
      DioError err, ErrorInterceptorHandler handler) async {
    try {
      final requestOptions = err.requestOptions;
      requestOptions.headers.remove('Authorization');
      requestOptions.headers.addAll(
          {'Authorization': 'Bearer ${accessTokenModel!.access_token}'});
      final result = await Dio().request(
        requestOptions.path,
        data: requestOptions.data,
        options: Options(
          method: requestOptions.method,
          headers: requestOptions.headers,
        ),
        queryParameters: requestOptions.queryParameters,
      );

      handler.resolve(Response(
          requestOptions: requestOptions,
          data: result.data,
          statusCode: result.statusCode,
          statusMessage: result.statusMessage));
    } on DioError catch (e) {
      handler.reject(e);
    }
  }

  void __saveTokenAndProceedToBase() {
    SecureStorage.saveLocalData(
        key: StorageKeys.accessToken, data: accessTokenModel!.access_token);
    SecureStorage.saveLocalData(
        key: StorageKeys.refreshToken, data: accessTokenModel!.refresh_token!);
    getConnect.Get.offAllNamed(PagesRoute.welcomeRoute);
  }
}
