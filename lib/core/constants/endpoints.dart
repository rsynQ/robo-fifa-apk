const String baseUrl = 'https://fifa.robobet.app';
//const String baseUrl = 'http://192.168.18.16:8001';

abstract class Endpoints {
  static const String login = '$baseUrl/user/login';
  static const String create = '$baseUrl/user/create';
  static const String logout = '$baseUrl/user/logout';
  static const String confirm = '$baseUrl/user/confirm';
  static const String refresh = '$baseUrl/user/refresh';
  static const String me = '$baseUrl/user/me';
  static const String matches = '$baseUrl/match/show';
  static const String showMatch = '$baseUrl/match/showMatch';
  static const String createBot = '$baseUrl/bot/create';
  static const String deleteBot = '$baseUrl/bot/delete';
  static const String bots = '$baseUrl/bot/show';
  static const String botDetails = '$baseUrl/bot/details';
  static const String alerts = '$baseUrl/bot/alerts';
  static const String players = '$baseUrl/player/show';
}
