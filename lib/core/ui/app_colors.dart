import 'package:flutter/material.dart';

class AppColors {
  static Color primaryColor = const Color(0xFF1B1B1B);
  static Color secundaryColor = const Color(0xFFDFDFDF);
  static Color textColor = const Color(0xFF2CE45A);
  static Color textColorWhite = const Color(0xFFFFFFFF);
  static Color greyColor = const Color(0xFF494949);
  static Color textBlack = const Color(0xFF000000);
}
