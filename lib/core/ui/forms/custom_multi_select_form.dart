import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import '../app_colors.dart';

class CustomMultiSelectForm extends StatelessWidget {
  final String label;
  final void Function(List<String>?)? onSaved;
  final void Function(List<String>) onConfirm;
  final List<String> initialValue;
  final List<MultiSelectItem<String>> items;
  const CustomMultiSelectForm(
      {super.key,
      required this.label,
      this.onSaved,
      required this.onConfirm,
      required this.items,
      required this.initialValue});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: MultiSelectDialogField(
        title: Text(label,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 12,
            )),
        buttonText: Text(label,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 12,
            )),
        decoration: BoxDecoration(
          color: AppColors.secundaryColor,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
        ),
        selectedColor: AppColors.primaryColor,
        onSaved: onSaved,
        onConfirm: onConfirm,
        initialValue: initialValue,
        cancelText: Text('CANCELAR',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 12,
                color: Colors.red.shade400)),
        confirmText: Text('CONFIRMAR',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 12,
                color: AppColors.primaryColor)),
        items: items,
      ),
    );
  }
}
