import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../app_colors.dart';

class CustomTextFormLabel extends StatelessWidget {
  final String label;
  final String? initialValue;
  final void Function(String?)? onSaved;
  final TextEditingController? controller;
  final List<TextInputFormatter>? inputFormatters;
  final FocusNode? focusNode;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  const CustomTextFormLabel(
      {super.key,
      required this.label,
      this.initialValue,
      this.onSaved,
      this.controller,
      this.inputFormatters,
      this.focusNode,
      this.validator,
      this.keyboardType});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                color: Color(0xFF46C48F),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10)),
              ),
              padding: const EdgeInsets.all(10.0),
              child: Text(
                'ODD: ',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  color: AppColors.textColorWhite,
                  fontSize: 14.0,
                ),
              ),
            ),
          ],
        ),
        Column(
          children: [
            SizedBox(
              height: 45,
              child: TextFormField(
                keyboardType: keyboardType,
                initialValue: initialValue,
                onSaved: onSaved,
                controller: controller,
                inputFormatters: inputFormatters,
                focusNode: focusNode,
                validator: validator,
                style: const TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
                decoration: InputDecoration(
                  labelText: label,
                  labelStyle:
                      const TextStyle(fontSize: 10, fontFamily: 'Roboto'),
                  filled: true,
                  fillColor: AppColors.secundaryColor,
                  focusColor: AppColors.textColor,
                  hoverColor: AppColors.textColor,
                  isDense: true,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
