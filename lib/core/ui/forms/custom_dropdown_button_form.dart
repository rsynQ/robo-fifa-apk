import 'package:flutter/material.dart';

import '../app_colors.dart';

class CustomDropdownButtonForm extends StatefulWidget {
  final String label;
  final List<DropdownMenuItem<int>>? items;
  final int? value;
  final void Function(int?)? onChanged;
  final void Function(int?)? onSaved;
  const CustomDropdownButtonForm(
      {super.key,
      required this.label,
      this.items,
      this.value,
      this.onChanged,
      this.onSaved});

  @override
  State<CustomDropdownButtonForm> createState() =>
      _CustomDropdownButtonFormState();
}

class _CustomDropdownButtonFormState extends State<CustomDropdownButtonForm> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                decoration: const BoxDecoration(
                  color: Color(0xFF46C48F),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                ),
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  widget.label,
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: AppColors.textColorWhite,
                    fontSize: 14.0,
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  height: 45,
                  child: DropdownButtonFormField(
                    value: widget.value,
                    dropdownColor: AppColors.secundaryColor,
                    items: widget.items,
                    onChanged: widget.onChanged,
                    onSaved: widget.onSaved,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                    ),
                    iconEnabledColor: Colors.black,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.secundaryColor,
                      focusColor: AppColors.textColor,
                      hoverColor: AppColors.textColor,
                      isDense: true,
                      border: const OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
