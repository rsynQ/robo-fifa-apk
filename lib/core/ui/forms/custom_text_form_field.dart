import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../app_colors.dart';

class CustomTextFormField extends StatefulWidget {
  final bool isSecret;
  final String label;
  final String? initialValue;
  final void Function(String?)? onSaved;
  final TextEditingController? controller;
  final List<TextInputFormatter>? inputFormatters;
  final FocusNode? focusNode;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  final EdgeInsetsGeometry? padding;
  final double? height;
  final int? maxLength;
  const CustomTextFormField({
    super.key,
    this.initialValue,
    this.onSaved,
    this.controller,
    this.inputFormatters,
    this.focusNode,
    this.validator,
    this.keyboardType,
    required this.label,
    this.padding = const EdgeInsets.all(10.0),
    this.height = 45,
    this.isSecret = false,
    this.maxLength,
  });

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool isObscure = false;

  @override
  void initState() {
    isObscure = widget.isSecret;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding!,
      child: TextFormField(
        keyboardType: widget.keyboardType,
        obscureText: isObscure,
        maxLength: widget.maxLength,
        initialValue: widget.initialValue,
        onSaved: widget.onSaved,
        controller: widget.controller,
        inputFormatters: widget.inputFormatters,
        focusNode: widget.focusNode,
        validator: widget.validator,
        style: const TextStyle(
            fontFamily: 'Roboto', fontWeight: FontWeight.bold, fontSize: 12),
        decoration: InputDecoration(
          helperStyle: const TextStyle(
              color: Colors.white,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold),
          errorStyle: const TextStyle(
              color: Colors.red,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold),
          labelText: widget.label,
          labelStyle: const TextStyle(fontSize: 10, fontFamily: 'Roboto'),
          hintStyle: const TextStyle(color: Colors.green),
          focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
          ),
          suffixIcon: widget.isSecret
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      isObscure = !isObscure;
                    });
                  },
                  icon:
                      Icon(isObscure ? Icons.visibility : Icons.visibility_off))
              : null,
          filled: true,
          fillColor: AppColors.secundaryColor,
          focusColor: AppColors.textColor,
          hoverColor: AppColors.textColor,
          isDense: true,
          border: const OutlineInputBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10))),
        ),
      ),
    );
  }
}
