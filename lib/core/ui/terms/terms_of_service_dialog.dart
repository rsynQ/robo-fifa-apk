import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TermsOfServiceDialog extends StatelessWidget {
  const TermsOfServiceDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Termos de Uso e LGPD'),
      content: const SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Bem-vindo ao nosso aplicativo de criação de bots e recebimento de dicas para apostas esportivas!',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
              'Ao utilizar este aplicativo, você concorda com os seguintes termos:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
              '- Este aplicativo permite que os usuários criem bots e recebam notificações de dicas para fazer entradas em casas de apostas.',
            ),
            Text(
              '- O aplicativo também fornece estatísticas dos jogadores.',
            ),
            Text(
              '- Não nos responsabilizamos por qualquer eventualidade decorrente das entradas que os jogadores fazem por conta própria.',
            ),
            Text(
              '- Os usuários devem ter idade legal para jogar e devem obedecer às leis de apostas em sua jurisdição.',
            ),
            SizedBox(height: 10),
            Text(
              'Além disso, ao utilizar o aplicativo, você concorda com a nossa política de privacidade, em conformidade com a Lei Geral de Proteção de Dados (LGPD).',
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(onPressed: () => Get.back(), child: const Text('FECHAR'))
      ],
    );
  }
}
