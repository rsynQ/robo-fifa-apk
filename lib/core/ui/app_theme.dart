import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppTheme {
  static ThemeData lightTheme = ThemeData(
      useMaterial3: true,
      scaffoldBackgroundColor: AppColors.secundaryColor,
      appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(
            color: AppColors.textColor,
          ),
          backgroundColor: AppColors.primaryColor,
          centerTitle: true),
      cardTheme: CardTheme(
        elevation: 1,
        color: AppColors.secundaryColor,
        surfaceTintColor: AppColors.secundaryColor,
        shadowColor: Colors.grey.shade500,
      ),
      textTheme: const TextTheme(
        bodyLarge: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 16.0,
          color: Colors.black,
        ),
        bodyMedium: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 14.0,
          color: Colors.black,
        ),
      ));
}
