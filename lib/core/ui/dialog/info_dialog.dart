import 'package:flutter/material.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';

class InfoDialog extends StatelessWidget {
  final String title;
  final List<Widget> children;
  const InfoDialog({super.key, required this.title, required this.children});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      alignment: Alignment.center,
      backgroundColor: AppColors.primaryColor,
      title: Text(title,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: AppColors.textColor,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.bold,
          )),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: children,
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text('Fechar',
              style: TextStyle(
                color: AppColors.textColor,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold,
              )),
        ),
      ],
    );
  }
}
