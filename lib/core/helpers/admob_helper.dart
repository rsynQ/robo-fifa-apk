// ignore_for_file: non_constant_identifier_names

import 'dart:io';

import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdmobHelper {
  BannerAd? bannerAd;

  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-5480102849026299/7723365084";
    } else if (Platform.isIOS) {
      return "ca-app-pub-5480102849026299/7723365084";
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }

  static String get InterstitialAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-5480102849026299/2750240005";
    } else if (Platform.isIOS) {
      return "ca-app-pub-5480102849026299/2750240005";
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }
}
