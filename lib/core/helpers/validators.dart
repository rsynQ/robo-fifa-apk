import 'package:get/get.dart';

String? dataValidator(String? data) {
  if (data == null) {
    return 'Campo obrigatorio';
  }
  if (data.isEmpty) {
    return 'Campo obrigatorio';
  }

  if (data == '-') {
    return 'Campo obrigatorio';
  }

  return null;
}

String? dropdownValidator(int? dropdown) {
  if (dropdown == null) {
    return 'Campo obrigatorio';
  }
  if (dropdown == 0) {
    return 'Campo obrigatorio';
  }
  return null;
}

String? churchValidator(int? churchID) {
  if (churchID == null) {
    return 'Você precisa escolher uma congregação.';
  }
  if (churchID == 0) {
    return 'Você precisa escolher uma congregação.';
  }
  return null;
}

String? emailValidator(String? email) {
  if (email == null || email.isEmpty) {
    return 'Digite seu email!';
  }

  if (!email.isEmail) return 'Digite um email válido!';

  return null;
}

String? passwordValidator(password) {
  if (password == null || password.isEmpty) {
    return 'Digite sua senha!';
  }

  if (password.length < 7) {
    return 'Digite uma senha com pelos menos 7 caracteres.';
  }

  return null;
}

String? nameValidator(String? name) {
  if (name == null || name.isEmpty) {
    return 'Digite um nome!';
  }

  final names = name.split(' ');

  if (names.length == 1) return 'Digite seu nome e último nome!';

  return null;
}

String? phoneValidator(String? phone) {
  if (phone == null || phone.isEmpty) {
    return 'Digite um celular!';
  }

  return null;
}

String? numberValidator(String? number) {
  if (number == null || number.isEmpty) {
    return 'Digite um valor';
  }

  if (!number.isNum) return 'Digite um valor válido!';

  return null;
}

String? cpfvalidator(String? cpf) {
  if (cpf == null || cpf.isEmpty) {
    return 'Digite um CPF!';
  }

  if (!cpf.isCpf) return 'Digite um CPF válido!';

  return null;
}
