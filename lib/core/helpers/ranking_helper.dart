import '../../models/match/match_model.dart';
import '../../models/ranking/ranking_model.dart';

class RankingHelper {
  static String playerName(List<MatchModel> matches, int playerId) {
    String name = 'N/A';
    final match = matches[0];

    if (match.home_player_id == playerId) {
      name = match.home;
    } else if (match.away_player_id == playerId) {
      name = match.away;
    }

    return name;
  }

  static int countPlayerWins(List<MatchModel> matches, int playerId) {
    return matches
        .where((match) =>
            match.home_player_id == playerId && match.home_winner == 1 ||
            match.away_player_id == playerId && match.away_winner == 1)
        .length;
  }

  static int countPlayerLoss(List<MatchModel> matches, int playerId) {
    return matches
        .where((match) =>
            match.home_player_id == playerId && match.away_winner == 1 ||
            match.away_player_id == playerId && match.home_winner == 1)
        .length;
  }

  static int countPlayerGoals(List<MatchModel> matches, int playerId) {
    int totalGoals = 0;

    for (var match in matches) {
      if (match.home_player_id == playerId) {
        totalGoals += match.home_goals_ft;
      }
      if (match.away_player_id == playerId) {
        totalGoals += match.away_goals_ft;
      }
    }

    return totalGoals;
  }

  static int countPlayerGoalsConceded(List<MatchModel> matches, int playerId) {
    int totalGoalsConceded = 0;

    for (var match in matches) {
      if (match.home_player_id == playerId) {
        totalGoalsConceded += match.away_goals_ft;
      }
      if (match.away_player_id == playerId) {
        totalGoalsConceded += match.home_goals_ft;
      }
    }

    return totalGoalsConceded;
  }

  static double countMatchesGoalByPlayer(
      List<MatchModel> matches, int playerId, double goal) {
    int matchesWithMoreThanHalfGoalByPlayer = 0;
    int playerGames = 0;

    for (var match in matches) {
      if ((match.home_player_id == playerId && match.home_goals_ft > goal) ||
          (match.away_player_id == playerId && match.away_goals_ft > goal)) {
        matchesWithMoreThanHalfGoalByPlayer++;
      }
    }

    playerGames = matches.length;

    if (playerGames == 0) {
      return 0.0;
    }

    return ((matchesWithMoreThanHalfGoalByPlayer / playerGames) * 100)
        .ceilToDouble();
  }

  static double countMatchesGoalHtByPlayer(
      List<MatchModel> matches, int playerId, double goal) {
    int matchesWithMoreThanHalfGoalByPlayer = 0;
    int playerGames = 0;

    for (var match in matches) {
      if ((match.home_player_id == playerId && match.home_goals_ht > goal) ||
          (match.away_player_id == playerId && match.away_goals_ht > goal)) {
        matchesWithMoreThanHalfGoalByPlayer++;
      }
    }

    playerGames = matches.length;

    if (playerGames == 0) {
      return 0.0;
    }

    return ((matchesWithMoreThanHalfGoalByPlayer / playerGames) * 100)
        .ceilToDouble();
  }

  static double countMatchesBttsHt(List<MatchModel> matches) {
    int matchesWithMoreThanHalfGoalByPlayer = 0;
    int playerGames = 0;

    for (var match in matches) {
      if ((match.home_goals_ht > 0.5) || (match.away_goals_ht > 0.5)) {
        matchesWithMoreThanHalfGoalByPlayer++;
      }
    }

    playerGames = matches.length;

    if (playerGames == 0) {
      return 0.0;
    }

    return ((matchesWithMoreThanHalfGoalByPlayer / playerGames) * 100)
        .ceilToDouble();
  }

  static double countMatchesBtts(List<MatchModel> matches) {
    int matchesWithMoreThanHalfGoalByPlayer = 0;
    int playerGames = 0;

    for (var match in matches) {
      if ((match.home_goals_ft > 0.5) || (match.away_goals_ft > 0.5)) {
        matchesWithMoreThanHalfGoalByPlayer++;
      }
    }

    playerGames = matches.length;

    if (playerGames == 0) {
      return 0.0;
    }

    return ((matchesWithMoreThanHalfGoalByPlayer / playerGames) * 100)
        .ceilToDouble();
  }

  static int customSortFunction(RankingModel a, RankingModel b) {
    // Pesos atribuídos às métricas (ap, gp, sg)
    double weightAp = 0.6;
    double weightGp = 0.3;
    double weightSg = 0.1;

    // Calculando a pontuação geral para cada jogador
    double scoreA = (a.ap * weightAp) + (a.gp * weightGp) + (a.sg * weightSg);
    double scoreB = (b.ap * weightAp) + (b.gp * weightGp) + (b.sg * weightSg);

    // Classificando em ordem decrescente de pontuação
    return scoreB.compareTo(scoreA);
  }
}
