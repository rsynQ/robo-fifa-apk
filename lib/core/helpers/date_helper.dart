import 'package:intl/intl.dart';

class DateHelper {
  static DateTime convertTimestampToDateTime(int timestamp) {
    return DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  }

  static String formatDateTime(int timestamp,
      {String format = 'dd/MM/yyyy HH:mm'}) {
    final dateTime = convertTimestampToDateTime(timestamp);
    final formatter = DateFormat(format);
    return formatter.format(dateTime);
  }

  static String formatDateTimeUTC(String dateTimeString,
      {String format = 'dd/MM/yyyy HH:mm'}) {
    final dateTime = DateTime.parse(dateTimeString).toLocal();
    final formatter = DateFormat(format);
    return formatter.format(dateTime);
  }
}
