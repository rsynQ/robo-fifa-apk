import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:robo_fifa/core/helpers/admob_helper.dart';

class CustomBanner extends StatefulWidget {
  const CustomBanner({Key? key}) : super(key: key);

  @override
  State<CustomBanner> createState() => _CustomBannerState();
}

class _CustomBannerState extends State<CustomBanner> {
  late BannerAd bannerAd;
  bool isBannerAdLoaded = false;

  @override
  void initState() {
    super.initState();
    createBannerAd();
  }

  @override
  void dispose() {
    bannerAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    if (!isBannerAdLoaded) {
      return Container();
    }

    return Container(
      color: Colors.white,
      alignment: Alignment.center,
      width: size.width * .98,
      height: 60,
      child: AdWidget(ad: bannerAd),
    );
  }

  void createBannerAd() {
    bannerAd = BannerAd(
      adUnitId: AdmobHelper.bannerAdUnitId,
      size: AdSize.banner,
      request: const AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (ad) {
          setState(() {
            isBannerAdLoaded = true;
          });
        },
        onAdFailedToLoad: (ad, error) {
          ad.dispose();
        },
      ),
    );

    bannerAd.load();
  }
}
