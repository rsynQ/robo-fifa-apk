// ignore_for_file: depend_on_referenced_packages

import 'package:freezed_annotation/freezed_annotation.dart';

part 'data_string_result.freezed.dart';

@freezed
class DataStringResult with _$DataStringResult {
  factory DataStringResult.success(String data) = Success;
  factory DataStringResult.error(String message) = Error;
}
