// ignore_for_file: depend_on_referenced_packages

import 'package:freezed_annotation/freezed_annotation.dart';

part 'data_result.freezed.dart';

@freezed
class DataResult<T> with _$DataResult<T> {
  factory DataResult.success(List<T> data) = Success;
  factory DataResult.error(String message) = Error;
}
