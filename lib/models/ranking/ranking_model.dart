// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
class RankingModel {
  String name;
  int playerID;
  int j;
  int v;
  int d;
  int e;
  int gp;
  double mgp;
  int gc;
  double mgc;
  int sg;
  int p;
  double ap;
  double g05;
  double g15;
  double g25;
  double g35;
  double g45;
  double g05ht;
  double g15ht;
  double g25ht;
  double g35ht;
  double bts_ht;
  double bts_ft;
  RankingModel({
    required this.name,
    required this.playerID,
    required this.j,
    required this.v,
    required this.d,
    required this.e,
    required this.gp,
    required this.mgp,
    required this.gc,
    required this.mgc,
    required this.sg,
    required this.p,
    required this.ap,
    required this.g05,
    required this.g15,
    required this.g25,
    required this.g35,
    required this.g45,
    required this.g05ht,
    required this.g15ht,
    required this.g25ht,
    required this.g35ht,
    required this.bts_ht,
    required this.bts_ft,
  });
}
