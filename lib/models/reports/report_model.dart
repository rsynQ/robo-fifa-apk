import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
class ReportModel {
  String? bot_name;
  String home_name;
  String score;
  int? home_goals_ft;
  int home_player_id;
  String home_team;
  int home_team_id;
  String away_name;
  int away_player_id;
  int? away_goals_ft;
  String away_team;
  int away_team_id;
  int match_id;
  int minute;
  num line_home_odd;
  num line_away_odd;
  num line_odd;
  String handicap;
  int status;
  num roi;
  String created_at;
  ReportModel({
    this.bot_name,
    required this.score,
    required this.home_name,
    required this.home_player_id,
    this.home_goals_ft,
    required this.home_team,
    required this.home_team_id,
    required this.away_name,
    required this.away_player_id,
    this.away_goals_ft,
    required this.away_team,
    required this.away_team_id,
    required this.match_id,
    required this.minute,
    required this.line_home_odd,
    required this.line_away_odd,
    required this.line_odd,
    required this.handicap,
    required this.status,
    required this.roi,
    required this.created_at,
  });

  factory ReportModel.fromMap(Map<String, dynamic> map) {
    return ReportModel(
      bot_name: map['bot_name'] != null ? map['bot_name'] as String : '-',
      score: map['scoreboard'] as String,
      home_name: map['home_name'] as String,
      home_player_id: map['home_player_id'] as int,
      home_team: map['home_team'] as String,
      home_team_id: map['home_team_id'] as int,
      home_goals_ft:
          map['home_goals_ft'] != null ? map['home_goals_ft'] as int : null,
      away_name: map['away_name'] as String,
      away_player_id: map['away_player_id'] as int,
      away_goals_ft:
          map['away_goals_ft'] != null ? map['away_goals_ft'] as int : null,
      away_team: map['away_team'] as String,
      away_team_id: map['away_team_id'] as int,
      match_id: map['match_id'] as int,
      minute: map['minute'] as int,
      line_home_odd: map['line_home_odd'] as num,
      line_away_odd: map['line_away_odd'] as num,
      line_odd: map['line_odd'] as num,
      handicap: map['handicap'] as String,
      status: map['status'] != null ? map['status'] as int : -1,
      roi: map['roi'] != null ? map['roi'] as num : 0,
      created_at: map['created_at'] as String,
    );
  }

  factory ReportModel.fromJson(String source) =>
      ReportModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
