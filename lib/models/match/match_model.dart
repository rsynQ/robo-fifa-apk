import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
class MatchModel {
  String home;
  String away;
  String home_team;
  String away_team;
  int id;
  int league_id;
  String datetime;
  int home_player_id;
  int home_team_id;
  int home_first_score;
  int home_winner;
  int home_goals_ht;
  int home_goals_ft;
  int away_player_id;
  int away_team_id;
  int away_first_score;
  int away_winner;
  int away_goals_ht;
  int away_goals_ft;
  int btts_ht;
  int btts_ft;
  int draw;
  int finished;
  MatchModel({
    required this.home,
    required this.away,
    required this.home_team,
    required this.away_team,
    required this.id,
    required this.league_id,
    required this.datetime,
    required this.home_player_id,
    required this.home_team_id,
    required this.home_first_score,
    required this.home_winner,
    required this.home_goals_ht,
    required this.home_goals_ft,
    required this.away_player_id,
    required this.away_team_id,
    required this.away_first_score,
    required this.away_winner,
    required this.away_goals_ht,
    required this.away_goals_ft,
    required this.btts_ht,
    required this.btts_ft,
    required this.draw,
    required this.finished,
  });

  factory MatchModel.fromMap(Map<String, dynamic> map) {
    return MatchModel(
      home: map['home'] as String,
      away: map['away'] as String,
      home_team: map['home_team'] as String,
      away_team: map['away_team'] as String,
      id: map['id'] as int,
      league_id: map['league_id'] as int,
      datetime: map['datetime'] as String,
      home_player_id: map['home_player_id'] as int,
      home_team_id: map['home_team_id'] as int,
      home_first_score:
          map['home_first_score'] != null ? map['home_first_score'] as int : 0,
      home_winner: map['home_winner'] != null ? map['home_winner'] as int : 0,
      home_goals_ht:
          map['home_goals_ht'] != null ? map['home_goals_ht'] as int : 0,
      home_goals_ft:
          map['home_goals_ft'] != null ? map['home_goals_ft'] as int : 0,
      away_player_id:
          map['away_player_id'] != null ? map['away_player_id'] as int : 0,
      away_team_id:
          map['away_team_id'] != null ? map['away_team_id'] as int : 0,
      away_first_score:
          map['away_first_score'] != null ? map['away_first_score'] as int : 0,
      away_winner: map['away_winner'] != null ? map['away_winner'] as int : 0,
      away_goals_ht:
          map['away_goals_ht'] != null ? map['away_goals_ht'] as int : 0,
      away_goals_ft:
          map['away_goals_ft'] != null ? map['away_goals_ft'] as int : 0,
      btts_ht: map['btts_ht'] != null ? map['btts_ht'] as int : 0,
      btts_ft: map['btts_ft'] != null ? map['btts_ft'] as int : 0,
      draw: map['draw'] != null ? map['draw'] as int : 0,
      finished: map['finished'] as int,
    );
  }

  factory MatchModel.fromJson(String source) =>
      MatchModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
