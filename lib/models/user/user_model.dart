import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
class UserModel {
  int? id;
  String username;
  String email;
  String? password;
  String phone;
  String register_type;
  String? chat_id;
  int? pro;
  int? transaction_date;
  UserModel({
    this.id,
    required this.username,
    required this.email,
    this.password,
    required this.phone,
    required this.register_type,
    this.chat_id,
    this.transaction_date,
    this.pro,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'username': username,
      'email': email,
      'password': password,
      'phone': phone,
      'register_type': register_type,
      'chat_id': chat_id,
      'transaction_date': transaction_date,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      id: map['id'] != null ? map['id'] as int : null,
      username: map['username'] as String,
      email: map['email'] as String,
      password: map['password'] != null ? map['password'] as String : null,
      phone: map['phone'] as String,
      register_type: map['register_type'] as String,
      chat_id: map['chat_id'] != null ? map['chat_id'] as String : null,
      pro: map['pro'] != null ? map['pro'] as int : 0,
      transaction_date: map['transaction_date'] != null
          ? map['transaction_date'] as int
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
