// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

// ignore_for_file: non_constant_identifier_names

class ConditionModel {
  int? type;
  int? type_avg;
  int? player;
  int? modality;
  int? difference;
  int? condition;
  num? average;
  int? consider;
  ConditionModel({
    this.type,
    this.type_avg,
    this.player,
    this.modality,
    this.difference,
    this.condition,
    this.average,
    this.consider,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'type': type,
      'type_avg': type_avg,
      'player': player,
      'modality': modality,
      'difference': difference,
      'condition': condition,
      'average': average,
      'consider': consider,
    };
  }

  factory ConditionModel.fromMap(Map<String, dynamic> map) {
    return ConditionModel(
      type: map['type'] != null ? map['type'] as int : null,
      type_avg: map['type_avg'] != null ? map['type_avg'] as int : null,
      player: map['player'] != null ? map['player'] as int : null,
      modality: map['modality'] != null ? map['modality'] as int : null,
      difference: map['difference'] != null ? map['difference'] as int : null,
      condition: map['condition'] != null ? map['condition'] as int : null,
      average: map['average'] != null ? map['average'] as num : null,
      consider: map['consider'] != null ? map['consider'] as int : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ConditionModel.fromJson(String source) =>
      ConditionModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
