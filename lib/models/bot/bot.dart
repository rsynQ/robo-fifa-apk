// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

import 'condition_model.dart';

class Bot {
  int? id;
  String? bot_name;
  int? market;
  int? field_command;
  int? internal_line;
  List<String>? market_line;
  num? odd;
  num? stake;
  int? minute;
  num? qt;
  num? roi;
  num? odd_med;
  int? active;
  List<String>? leaderboards;
  int? bw_type;
  List<String>? bwlist;
  List<ConditionModel>? conditions;
  Bot({
    this.id,
    this.bot_name,
    this.market,
    this.field_command,
    this.internal_line,
    this.market_line,
    this.odd,
    this.stake,
    this.minute,
    this.leaderboards,
    this.bw_type,
    this.bwlist,
    this.conditions,
    this.qt,
    this.roi,
    this.odd_med,
    this.active,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'bot_name': bot_name,
      'market': market,
      'field_command': field_command,
      'internal_line': internal_line,
      'market_line': market_line,
      'odd': odd,
      'stake': stake,
      'minute': minute,
      'leaderboards': leaderboards,
      'bw_type': bw_type,
      'bwlist': bwlist,
      'active': active,
      'conditions': conditions?.map((x) => x.toMap()).toList(),
    };
  }

  factory Bot.fromMap(Map<String, dynamic> map) {
    return Bot(
      id: map['id'] != null ? map['id'] as int : null,
      bot_name: map['bot_name'] as String,
      market: map['market'] as int,
      field_command:
          map['field_command'] != null ? map['field_command'] as int : 0,
      internal_line:
          map['internal_line'] != null ? map['internal_line'] as int : null,
      market_line: map['market_line'] != null
          ? List<String>.from((map['market_line'] as List<dynamic>))
          : null,
      odd: map['odd'] != null ? map['odd'] as num : 0,
      qt: map['qt'] != null ? map['qt'] as num : 0,
      roi: map['roi'] != null ? map['roi'] as num : 0,
      odd_med: map['odd_med'] != null ? map['odd_med'] as num : 0,
      stake: map['stake'] != null ? map['stake'] as num : 50,
      minute: map['minute'] as int,
      active: map['active'] != null ? map['active'] as int : 0,
      bw_type: map['bw_type'] != null ? map['bw_type'] as int : 0,
      bwlist: map['bwlist'] != null
          ? List<String>.from((map['bwlist'] as List<dynamic>))
          : null,
      leaderboards: map['leaderboards'] != null
          ? List<String>.from((map['leaderboards'] as List<dynamic>))
          : null,
      conditions: map['conditions'] != null
          ? List<ConditionModel>.from(
              (map['conditions'] as List<dynamic>).map<ConditionModel>(
                (x) => ConditionModel.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Bot.fromJson(String source) =>
      Bot.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Bot(id: $id, bot_name: $bot_name, market: $market, field_command: $field_command, internal_line: $internal_line, market_line: $market_line, odd: $odd, stake: $stake, minute: $minute, qt: $qt, roi: $roi, odd_med: $odd_med, active: $active, leaderboards: $leaderboards, bw_type: $bw_type, bwlist: $bwlist, conditions: $conditions)';
  }
}
