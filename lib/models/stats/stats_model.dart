// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names

import 'dart:convert';

import 'goal_trend.dart';

class StatsModel {
  int matches;
  int winner;
  int loss;
  int draw;
  num winner_ap;
  int goals_ht;
  num goals_ht_avg;
  num goals_ht_ap;
  int goals_conceded_ht;
  num goals_conceded_ht_avg;
  num goals_conceded_ht_ap;
  num first_score;
  int goals;
  num goals_avg;
  int goals_conceded;
  num goals_conceded_avg;
  num goals_conceded_ap;
  num over_05ht;
  num over_15ht;
  num over_25ht;
  num over_35ht;
  num over_05;
  num over_15;
  num over_25;
  num over_35;
  num over_45;
  num btts_ht;
  num btts;
  num under_05ht;
  num under_15ht;
  num under_25ht;
  num under_35ht;
  num under_05;
  num under_15;
  num under_25;
  num under_35;
  num under_45;
  num under_btts_ht;
  num under_btts;
  num goal_ht0min;
  num goal_ht1min;
  num goal_ht2min;
  num goal_ht3min;
  num goal_ht4min;
  num goal_ht5min;
  num goal_ht6min;
  num goal_ht7min;
  num team_winner;
  num team_loss;
  num team_draw;
  GoalTrend? goal_trend;
  StatsModel({
    required this.matches,
    required this.winner,
    required this.loss,
    required this.draw,
    required this.winner_ap,
    required this.goals_ht,
    required this.goals_ht_avg,
    required this.goals_ht_ap,
    required this.goals_conceded_ht,
    required this.goals_conceded_ht_avg,
    required this.goals_conceded_ht_ap,
    required this.first_score,
    required this.goals,
    required this.goals_avg,
    required this.goals_conceded,
    required this.goals_conceded_avg,
    required this.goals_conceded_ap,
    required this.over_05ht,
    required this.over_15ht,
    required this.over_25ht,
    required this.over_35ht,
    required this.over_05,
    required this.over_15,
    required this.over_25,
    required this.over_35,
    required this.over_45,
    required this.btts_ht,
    required this.btts,
    required this.under_05ht,
    required this.under_15ht,
    required this.under_25ht,
    required this.under_35ht,
    required this.under_05,
    required this.under_15,
    required this.under_25,
    required this.under_35,
    required this.under_45,
    required this.under_btts_ht,
    required this.under_btts,
    required this.goal_ht0min,
    required this.goal_ht1min,
    required this.goal_ht2min,
    required this.goal_ht3min,
    required this.goal_ht4min,
    required this.goal_ht5min,
    required this.goal_ht6min,
    required this.goal_ht7min,
    required this.team_draw,
    required this.team_loss,
    required this.team_winner,
    this.goal_trend,
  });

  factory StatsModel.fromMap(Map<String, dynamic> map) {
    return StatsModel(
      matches: map['matches'] ?? 0,
      winner: map['winner'] ?? 0,
      loss: map['loss'] ?? 0,
      draw: map['draw'] ?? 0,
      winner_ap: map['winner_ap'] ?? 0,
      goals_ht: map['goals_ht'] ?? 0,
      goals_ht_avg: map['goals_ht_avg'] ?? 0,
      goals_ht_ap: map['goals_ht_ap'] ?? 0,
      goals_conceded_ht: map['goals_conceded_ht'] ?? 0,
      goals_conceded_ht_avg: map['goals_conceded_ht_avg'] ?? 0,
      goals_conceded_ht_ap: map['goals_conceded_ht_ap'] ?? 0,
      first_score: map['first_score'] ?? 0,
      goals: map['goals'] ?? 0,
      goals_avg: map['goals_avg'] ?? 0,
      goals_conceded: map['goals_conceded'] ?? 0,
      goals_conceded_avg: map['goals_conceded_avg'] ?? 0,
      goals_conceded_ap: map['goals_conceded_ap'] ?? 0,
      over_05ht: map['over_05ht'] ?? 0,
      over_15ht: map['over_15ht'] ?? 0,
      over_25ht: map['over_25ht'] ?? 0,
      over_35ht: map['over_35ht'] ?? 0,
      over_05: map['over_05'] ?? 0,
      over_15: map['over_15'] ?? 0,
      over_25: map['over_25'] ?? 0,
      over_35: map['over_35'] ?? 0,
      over_45: map['over_45'] ?? 0,
      btts_ht: map['btts_ht'] ?? 0,
      btts: map['btts'] ?? 0,
      under_05ht: map['under_05ht'] ?? 0,
      under_15ht: map['under_15ht'] ?? 0,
      under_25ht: map['under_25ht'] ?? 0,
      under_35ht: map['under_35ht'] ?? 0,
      under_05: map['under_05'] ?? 0,
      under_15: map['under_15'] ?? 0,
      under_25: map['under_25'] ?? 0,
      under_35: map['under_35'] ?? 0,
      under_45: map['under_45'] ?? 0,
      under_btts_ht: map['under_btts_ht'] ?? 0,
      under_btts: map['under_btts'] ?? 0,
      goal_ht0min: map['goal_ht0min'] ?? 0,
      goal_ht1min: map['goal_ht1min'] ?? 0,
      goal_ht2min: map['goal_ht2min'] ?? 0,
      goal_ht3min: map['goal_ht3min'] ?? 0,
      goal_ht4min: map['goal_ht4min'] ?? 0,
      goal_ht5min: map['goal_ht5min'] ?? 0,
      goal_ht6min: map['goal_ht6min'] ?? 0,
      goal_ht7min: map['goal_ht7min'] ?? 0,
      team_draw: map['team_draw'] ?? 0,
      team_loss: map['team_loss'] ?? 0,
      team_winner: map['team_winner'] ?? 0,
      goal_trend: map['goal_trend'] != null
          ? GoalTrend.fromMap(map['goal_trend'] as Map<String, dynamic>)
          : null,
    );
  }

  factory StatsModel.fromJson(String source) =>
      StatsModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
