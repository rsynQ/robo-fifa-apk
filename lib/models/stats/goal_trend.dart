import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first
class GoalTrend {
  List<num> total;
  List<num> home;
  List<num> away;
  GoalTrend({
    required this.total,
    required this.home,
    required this.away,
  });

  factory GoalTrend.fromMap(Map<String, dynamic> map) {
    return GoalTrend(
      total: (map['total'] as List<dynamic>).map((e) => e as num).toList(),
      home: (map['home'] as List<dynamic>).map((e) => e as num).toList(),
      away: (map['away'] as List<dynamic>).map((e) => e as num).toList(),
    );
  }

  factory GoalTrend.fromJson(String source) =>
      GoalTrend.fromMap(json.decode(source) as Map<String, dynamic>);
}
