// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:robo_fifa/models/stats/h2h_model.dart';
import 'package:robo_fifa/models/stats/stats_model.dart';

class AnalyzerModel {
  StatsModel home;
  StatsModel away;
  StatsModel resume;
  H2hModel h2h;
  AnalyzerModel({
    required this.home,
    required this.away,
    required this.resume,
    required this.h2h,
  });

  factory AnalyzerModel.fromMap(Map<String, dynamic> map) {
    return AnalyzerModel(
      home: StatsModel.fromMap(map['home'] as Map<String, dynamic>),
      away: StatsModel.fromMap(map['away'] as Map<String, dynamic>),
      resume: StatsModel.fromMap(map['resume'] as Map<String, dynamic>),
      h2h: H2hModel.fromMap(map['h2h'] as Map<String, dynamic>),
    );
  }

  factory AnalyzerModel.fromJson(String source) =>
      AnalyzerModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
