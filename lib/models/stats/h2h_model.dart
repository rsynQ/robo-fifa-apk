// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'stats_model.dart';

class H2hModel {
  StatsModel home;
  StatsModel away;
  StatsModel resume;
  H2hModel({
    required this.home,
    required this.away,
    required this.resume,
  });

  factory H2hModel.fromMap(Map<String, dynamic> map) {
    return H2hModel(
      home: StatsModel.fromMap(map['home'] as Map<String, dynamic>),
      away: StatsModel.fromMap(map['away'] as Map<String, dynamic>),
      resume: StatsModel.fromMap(map['resume'] as Map<String, dynamic>),
    );
  }

  factory H2hModel.fromJson(String source) =>
      H2hModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
