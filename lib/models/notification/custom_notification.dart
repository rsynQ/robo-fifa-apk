// ignore_for_file: public_member_api_docs, sort_constructors_first
class CustomNotification {
  int id;
  String? title;
  String? body;
  String? payload;
  CustomNotification({
    required this.id,
    this.title,
    this.body,
    this.payload,
  });
}
