import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
class TokenModel {
  String access_token;
  String? refresh_token;
  String? token_type;
  int? expires_in;
  TokenModel({
    required this.access_token,
    this.refresh_token,
    this.token_type,
    this.expires_in,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'access_token': access_token,
      'refresh_token': refresh_token,
      'token_type': token_type,
      'expires_in': expires_in,
    };
  }

  factory TokenModel.fromMap(Map<String, dynamic> map) {
    return TokenModel(
      access_token: map['access_token'] as String,
      refresh_token:
          map['refresh_token'] != null ? map['refresh_token'] as String : null,
      token_type:
          map['token_type'] != null ? map['token_type'] as String : null,
      expires_in: map['expires_in'] != null ? map['expires_in'] as int : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory TokenModel.fromJson(String source) =>
      TokenModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
