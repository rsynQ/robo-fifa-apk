// ignore_for_file: public_member_api_docs, sort_constructors_first
class ChartData {
  final num index;
  final double total;
  final double home;
  final double away;
  ChartData({
    required this.index,
    required this.total,
    required this.home,
    required this.away,
  });
}
