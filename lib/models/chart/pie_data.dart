// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:flutter/material.dart';

class PieData {
  final String xData;
  final num yData;
  String? text;
  final Color? color;
  PieData({
    required this.xData,
    required this.yData,
    this.text,
    this.color,
  });

  get roi => null;
}
