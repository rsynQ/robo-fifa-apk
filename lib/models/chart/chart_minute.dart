// ignore_for_file: public_member_api_docs, sort_constructors_first
class ChartMinute {
  final String x;
  final double y;
  final double z;
  final double a;
  ChartMinute({
    required this.x,
    required this.y,
    required this.z,
    required this.a,
  });
}
