import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';
import 'package:robo_fifa/pages/analyzer/widgets/char_circular_widget.dart';
import 'package:robo_fifa/pages/analyzer/widgets/char_goals_widget.dart';
import 'package:robo_fifa/pages/analyzer/widgets/chart_bar_minute_widget.dart';

import '../../../core/components/custom_app_bar.dart';
import '../../../core/helpers/date_helper.dart';
import '../controller/analyzer_controller.dart';
import '../widgets/chart_trend_widget.dart';
import '../widgets/single_circular_widget.dart';

class AnalyzerScreen extends GetView<AnalyzerController> {
  const AnalyzerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: GetBuilder<AnalyzerController>(
        builder: (controller) {
          return Column(
            children: [
              Card(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text(
                                controller.match.home,
                                style: const TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '(${controller.match.home_team})',
                                style: const TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        const Text(
                          'vs',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 10,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(width: 10),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text(
                                controller.match.away,
                                style: const TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '(${controller.match.away_team})',
                                style: const TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              DefaultTabController(
                length: 3,
                child: Expanded(
                  child: Column(
                    children: [
                      TabBar(
                        indicatorColor: AppColors.primaryColor,
                        indicatorSize: TabBarIndicatorSize.tab,
                        tabs: [
                          Tab(
                            child: Text(
                              'Visão Geral',
                              style: TextStyle(
                                color: AppColors.primaryColor,
                                fontFamily: 'Roboto',
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Tab(
                              child: Text(
                            controller.match.home,
                            style: TextStyle(
                              color: AppColors.primaryColor,
                              fontFamily: 'Roboto',
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          )),
                          Tab(
                              child: Text(
                            controller.match.away,
                            style: TextStyle(
                              color: AppColors.primaryColor,
                              fontFamily: 'Roboto',
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          )),
                        ],
                      ),
                      Expanded(
                        child: TabBarView(
                          children: [
                            ListView(
                              children: const [
                                CharCircularWidget(),
                                ChartTrendWidget(),
                                ChartBarMinuteWidget(),
                              ],
                            ),
                            ListView(
                              children: [
                                Visibility(
                                  child: SingleCircularWidget(
                                      label: controller.match.home_team,
                                      pieData: controller.teamHomeData),
                                ),
                                CharGoalsWidget(
                                  chartData: controller.matchesGoals(
                                      controller.match.home_player_id),
                                ),
                                Card(
                                  surfaceTintColor: AppColors.primaryColor,
                                  color: AppColors.primaryColor,
                                  elevation: 4,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          const SizedBox(width: 10),
                                          Text('Últimos jogos',
                                              style: TextStyle(
                                                  color:
                                                      AppColors.secundaryColor,
                                                  fontFamily: 'Roboto',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14)),
                                          const Spacer(),
                                          IconButton(
                                            onPressed: () {},
                                            icon: const Icon(Icons.info),
                                            color: AppColors.secundaryColor,
                                          ),
                                          const SizedBox(width: 10),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 300,
                                        child: ListView.builder(
                                          itemCount: controller
                                                      .listMatches(controller
                                                          .match.home_player_id)
                                                      .length >=
                                                  10
                                              ? 10
                                              : controller
                                                  .listMatches(controller
                                                      .match.home_player_id)
                                                  .length,
                                          itemBuilder: (context, index) {
                                            final match = controller
                                                .listMatches(controller.match
                                                    .home_player_id)[index];
                                            return Card(
                                              color: AppColors.greyColor,
                                              elevation: 1,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                              ),
                                              shadowColor: AppColors.textColor,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Row(
                                                  children: [
                                                    SizedBox(
                                                      width: 75,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            DateHelper
                                                                .formatDateTime(
                                                              int.parse(match
                                                                  .datetime),
                                                              format:
                                                                  'dd/MM/yyyy',
                                                            ),
                                                            style: TextStyle(
                                                              fontSize: 10,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                          Text(
                                                            DateHelper
                                                                .formatDateTime(
                                                              int.parse(match
                                                                  .datetime),
                                                              format: 'HH:mm',
                                                            ),
                                                            style: TextStyle(
                                                              fontSize: 10,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 120,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            match.home,
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 12,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                          Text(
                                                            match.home_team,
                                                            style: TextStyle(
                                                              fontSize: 11,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    const SizedBox(width: 5),
                                                    Column(
                                                      children: [
                                                        Text(
                                                          '(${match.home_goals_ht})',
                                                          style: TextStyle(
                                                            fontSize: 9,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                        Text(
                                                          '${match.home_goals_ft}',
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 10,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Text(
                                                      ' x ',
                                                      style: TextStyle(
                                                        fontSize: 11,
                                                        color: AppColors
                                                            .secundaryColor,
                                                        fontFamily: 'Roboto',
                                                      ),
                                                    ),
                                                    Column(
                                                      children: [
                                                        Text(
                                                          '(${match.away_goals_ht})',
                                                          style: TextStyle(
                                                            fontSize: 9,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                        Text(
                                                          '${match.away_goals_ft}',
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 10,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(width: 5),
                                                    SizedBox(
                                                      width: 120,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            match.away,
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 12,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                          Text(
                                                            match.away_team,
                                                            style: TextStyle(
                                                              fontSize: 11,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: (match.home_player_id ==
                                                                  controller
                                                                      .match
                                                                      .home_player_id &&
                                                              match.home_winner >
                                                                  0) ||
                                                          match.away_player_id ==
                                                                  controller
                                                                      .match
                                                                      .home_player_id &&
                                                              match.away_winner >
                                                                  0,
                                                      child: const Badge(
                                                        backgroundColor:
                                                            Colors.green,
                                                        label: Text('V'),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: (match.home_player_id ==
                                                                  controller
                                                                      .match
                                                                      .home_player_id &&
                                                              match.away_winner >
                                                                  0) ||
                                                          match.away_player_id ==
                                                                  controller
                                                                      .match
                                                                      .home_player_id &&
                                                              match.home_winner >
                                                                  0,
                                                      child: const Badge(
                                                        backgroundColor:
                                                            Colors.red,
                                                        label: Text('D'),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: (match
                                                                  .away_winner ==
                                                              0) &&
                                                          match.home_winner ==
                                                              0,
                                                      child: const Badge(
                                                        backgroundColor:
                                                            Colors.grey,
                                                        label: Text('E'),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                      const SizedBox(height: 10),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            ListView(
                              children: [
                                Visibility(
                                  child: SingleCircularWidget(
                                      label: controller.match.away_team,
                                      pieData: controller.teamAwayData),
                                ),
                                CharGoalsWidget(
                                  chartData: controller.matchesGoals(
                                      controller.match.away_player_id),
                                ),
                                Card(
                                  surfaceTintColor: AppColors.primaryColor,
                                  color: AppColors.primaryColor,
                                  elevation: 4,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          const SizedBox(width: 10),
                                          Text('Últimos jogos',
                                              style: TextStyle(
                                                  color:
                                                      AppColors.secundaryColor,
                                                  fontFamily: 'Roboto',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14)),
                                          const Spacer(),
                                          IconButton(
                                            onPressed: () {},
                                            icon: const Icon(Icons.info),
                                            color: AppColors.secundaryColor,
                                          ),
                                          const SizedBox(width: 10),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 300,
                                        child: ListView.builder(
                                          itemCount: controller
                                                      .listMatches(controller
                                                          .match.away_player_id)
                                                      .length >=
                                                  10
                                              ? 10
                                              : controller
                                                  .listMatches(controller
                                                      .match.away_player_id)
                                                  .length,
                                          itemBuilder: (context, index) {
                                            final match = controller
                                                .listMatches(controller.match
                                                    .away_player_id)[index];
                                            return Card(
                                              color: AppColors.greyColor,
                                              elevation: 1,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                              ),
                                              shadowColor: AppColors.textColor,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Row(
                                                  children: [
                                                    SizedBox(
                                                      width: 75,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            DateHelper
                                                                .formatDateTime(
                                                              int.parse(match
                                                                  .datetime),
                                                              format:
                                                                  'dd/MM/yyyy',
                                                            ),
                                                            style: TextStyle(
                                                              fontSize: 10,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                          Text(
                                                            DateHelper
                                                                .formatDateTime(
                                                              int.parse(match
                                                                  .datetime),
                                                              format: 'HH:mm',
                                                            ),
                                                            style: TextStyle(
                                                              fontSize: 10,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 120,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            match.home,
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 12,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                          Text(
                                                            match.home_team,
                                                            style: TextStyle(
                                                              fontSize: 11,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    const SizedBox(width: 5),
                                                    Column(
                                                      children: [
                                                        Text(
                                                          '(${match.home_goals_ht})',
                                                          style: TextStyle(
                                                            fontSize: 9,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                        Text(
                                                          '${match.home_goals_ft}',
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 10,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Text(
                                                      ' x ',
                                                      style: TextStyle(
                                                        fontSize: 11,
                                                        color: AppColors
                                                            .secundaryColor,
                                                        fontFamily: 'Roboto',
                                                      ),
                                                    ),
                                                    Column(
                                                      children: [
                                                        Text(
                                                          '(${match.away_goals_ht})',
                                                          style: TextStyle(
                                                            fontSize: 9,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                        Text(
                                                          '${match.away_goals_ft}',
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 10,
                                                            color: AppColors
                                                                .secundaryColor,
                                                            fontFamily:
                                                                'Roboto',
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(width: 5),
                                                    SizedBox(
                                                      width: 120,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            match.away,
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 12,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          ),
                                                          Text(
                                                            match.away_team,
                                                            style: TextStyle(
                                                              fontSize: 11,
                                                              color: AppColors
                                                                  .secundaryColor,
                                                              fontFamily:
                                                                  'Roboto',
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: (match.home_player_id ==
                                                                  controller
                                                                      .match
                                                                      .away_player_id &&
                                                              match.home_winner >
                                                                  0) ||
                                                          match.away_player_id ==
                                                                  controller
                                                                      .match
                                                                      .away_player_id &&
                                                              match.away_winner >
                                                                  0,
                                                      child: const Badge(
                                                        backgroundColor:
                                                            Colors.green,
                                                        label: Text('V'),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: (match.home_player_id ==
                                                                  controller
                                                                      .match
                                                                      .away_player_id &&
                                                              match.away_winner >
                                                                  0) ||
                                                          match.away_player_id ==
                                                                  controller
                                                                      .match
                                                                      .away_player_id &&
                                                              match.home_winner >
                                                                  0,
                                                      child: const Badge(
                                                        backgroundColor:
                                                            Colors.red,
                                                        label: Text('D'),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: (match
                                                                  .away_winner ==
                                                              0) &&
                                                          match.home_winner ==
                                                              0,
                                                      child: const Badge(
                                                        backgroundColor:
                                                            Colors.grey,
                                                        label: Text('E'),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                      const SizedBox(height: 10),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
