import 'package:get/get.dart';
import 'package:robo_fifa/pages/analyzer/controller/analyzer_controller.dart';

class AnalyzerBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(AnalyzerController());
  }
}
