// ignore: depend_on_referenced_packages
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:robo_fifa/models/stats/analyzer_model.dart';

part 'analyzer_result.freezed.dart';

@freezed
class AnalyzerResult with _$AnalyzerResult {
  factory AnalyzerResult.success(AnalyzerModel data) = Success;
  factory AnalyzerResult.error(String message) = Error;
}
