import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/models/chart/chart_minute.dart';

import '../../../core/auth/controller/auth_controller.dart';
import '../../../models/chart/chart_data.dart';
import '../../../models/chart/pie_data.dart';
import '../../../models/match/match_model.dart';
import '../../../models/stats/analyzer_model.dart';
import '../repository/analyzer_repository.dart';

class AnalyzerController extends GetxController {
  final auth = Get.find<AuthController>();
  late MatchModel match;
  late AnalyzerModel analyzer;

  final repository = AnalyzerRepository();

  List<ChartData> chartData = [];
  List<ChartMinute> chartMinute = [];
  List<PieData> pieDataHome = [];
  List<PieData> pieDataH2H = [];
  List<PieData> pieDataAway = [];
  List<PieData> teamHomeData = [];
  List<PieData> teamAwayData = [];

  @override
  void onInit() async {
    match = Get.arguments;
    await getMatchDetails();
    super.onInit();
  }

  List<ChartData> matchesGoals(int playerID) {
    List<ChartData> goals = [];
    final matches = listMatches(playerID).take(20);

    int index = matches.length;
    for (var e in matches) {
      if (e.home_player_id == playerID) {
        goals.add(ChartData(
            index: index,
            total: (e.home_goals_ft + e.home_goals_ft).toDouble(),
            home: e.home_goals_ft.toDouble(),
            away: e.away_goals_ft.toDouble()));
        index--;
      } else if (e.away_player_id == playerID) {
        goals.add(ChartData(
            index: index,
            total: (e.home_goals_ft + e.home_goals_ft).toDouble(),
            home: e.away_goals_ft.toDouble(),
            away: e.home_goals_ft.toDouble()));
        index--;
      }
    }

    return goals;
  }

  List<MatchModel> listMatches(int playerID) {
    return auth.matchesClosed
        .where((e) =>
            (e.home_player_id == playerID || e.away_player_id == playerID))
        .toList();
  }

  void treatChart() {
    chartData.clear();
    chartMinute.clear();

    int count = (analyzer.resume.goal_trend?.total.length ?? 0);
    for (var i = 0; i < count; i++) {
      double total = analyzer.resume.goal_trend!.total[i].toDouble();
      double home = analyzer.resume.goal_trend!.home[i].toDouble();
      double away = analyzer.resume.goal_trend!.away[i].toDouble();
      chartData.add(ChartData(index: i, total: total, home: home, away: away));
    }

    chartMinute.add(ChartMinute(
        x: '0-1',
        y: analyzer.home.goal_ht0min.toDouble(),
        z: analyzer.away.goal_ht0min.toDouble(),
        a: analyzer.h2h.resume.goal_ht0min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '1-2',
        y: analyzer.home.goal_ht1min.toDouble(),
        z: analyzer.away.goal_ht1min.toDouble(),
        a: analyzer.h2h.resume.goal_ht1min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '2-3',
        y: analyzer.home.goal_ht2min.toDouble(),
        z: analyzer.away.goal_ht2min.toDouble(),
        a: analyzer.h2h.resume.goal_ht2min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '3-4',
        y: analyzer.home.goal_ht3min.toDouble(),
        z: analyzer.away.goal_ht3min.toDouble(),
        a: analyzer.h2h.resume.goal_ht3min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '4-5',
        y: analyzer.home.goal_ht4min.toDouble(),
        z: analyzer.away.goal_ht4min.toDouble(),
        a: analyzer.h2h.resume.goal_ht4min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '5-6',
        y: analyzer.home.goal_ht5min.toDouble(),
        z: analyzer.away.goal_ht5min.toDouble(),
        a: analyzer.h2h.resume.goal_ht5min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '6-7',
        y: analyzer.home.goal_ht6min.toDouble(),
        z: analyzer.away.goal_ht6min.toDouble(),
        a: analyzer.h2h.resume.goal_ht6min.toDouble()));

    chartMinute.add(ChartMinute(
        x: '7+',
        y: analyzer.home.goal_ht7min.toDouble(),
        z: analyzer.away.goal_ht7min.toDouble(),
        a: analyzer.h2h.resume.goal_ht7min.toDouble()));

    pieDataHome.add(PieData(
        xData: 'Win', yData: analyzer.home.winner, color: Colors.green));
    pieDataHome.add(
        PieData(xData: 'Loss', yData: analyzer.home.loss, color: Colors.red));
    pieDataHome.add(
        PieData(xData: 'Draw', yData: analyzer.home.draw, color: Colors.grey));

    pieDataAway.add(PieData(
        xData: 'Win', yData: analyzer.away.winner, color: Colors.green));
    pieDataAway.add(
        PieData(xData: 'Loss', yData: analyzer.away.loss, color: Colors.red));
    pieDataAway.add(
        PieData(xData: 'Draw', yData: analyzer.away.draw, color: Colors.grey));

    pieDataH2H.add(PieData(
        xData: 'Win',
        yData: analyzer.h2h.home.winner,
        color: Colors.pink,
        text: '${analyzer.h2h.home.winner} Vit.'));
    pieDataH2H.add(PieData(
        xData: 'Loss',
        yData: analyzer.h2h.away.winner,
        color: Colors.blue,
        text: '${analyzer.h2h.away.winner} Vit.'));
    pieDataH2H.add(PieData(
        xData: 'Draw',
        yData: analyzer.h2h.home.draw,
        color: Colors.amber,
        text: '${analyzer.h2h.home.draw} Emp.'));

    teamHomeData.add(PieData(
        xData: 'Vitorias',
        yData: analyzer.home.team_winner,
        color: Colors.green));
    teamHomeData.add(PieData(
        xData: 'Derrotas', yData: analyzer.home.team_loss, color: Colors.red));
    teamHomeData.add(PieData(
        xData: 'Empates', yData: analyzer.home.team_draw, color: Colors.grey));

    teamAwayData.add(PieData(
        xData: 'Vitorias',
        yData: analyzer.away.team_winner,
        color: Colors.green));
    teamAwayData.add(PieData(
        xData: 'Derrotas', yData: analyzer.away.team_loss, color: Colors.red));
    teamAwayData.add(PieData(
        xData: 'Empates', yData: analyzer.away.team_draw, color: Colors.grey));
  }

  Future<void> getMatchDetails() async {
    EasyLoading.show(status: 'Coletando informações');
    final response = await repository.getMatchDetails(match.id);
    response.when(
        success: (data) async {
          analyzer = data;
          treatChart();
          update();
        },
        error: (message) {});
    EasyLoading.dismiss();
  }
}
