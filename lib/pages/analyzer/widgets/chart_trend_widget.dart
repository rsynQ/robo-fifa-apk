import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/pages/analyzer/controller/analyzer_controller.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../core/ui/app_colors.dart';
import '../../../core/ui/dialog/info_dialog.dart';
import '../../../models/chart/chart_data.dart';

class ChartTrendWidget extends GetView<AnalyzerController> {
  const ChartTrendWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AnalyzerController>(
      builder: (controller) {
        return Card(
          surfaceTintColor: AppColors.primaryColor,
          color: AppColors.primaryColor,
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  const SizedBox(width: 10),
                  Text('Tendência de Gols',
                      style: TextStyle(
                          color: AppColors.secundaryColor,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold,
                          fontSize: 14)),
                  const Spacer(),
                  IconButton(
                    onPressed: () => Get.dialog(
                      InfoDialog(
                        title: 'IMPORTANTE',
                        children: [
                          Row(
                            children: [
                              Container(
                                  width: 20, height: 20, color: Colors.amber),
                              const SizedBox(width: 10),
                              Text(
                                'TOTAL DE MÉDIAS',
                                style: TextStyle(
                                  color:
                                      AppColors.secundaryColor.withOpacity(.8),
                                  fontFamily: 'Roboto',
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Container(
                                  width: 20, height: 20, color: Colors.pink),
                              const SizedBox(width: 10),
                              Text(
                                'MÉDIAS DO MANDANTE',
                                style: TextStyle(
                                  color:
                                      AppColors.secundaryColor.withOpacity(.8),
                                  fontFamily: 'Roboto',
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Container(
                                  width: 20, height: 20, color: Colors.blue),
                              const SizedBox(width: 10),
                              Text(
                                'MÉDIAS DO VISITANTE',
                                style: TextStyle(
                                  color:
                                      AppColors.secundaryColor.withOpacity(.8),
                                  fontFamily: 'Roboto',
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 20),
                          Text(
                            'O gráfico exibe as médias de gols dos últimos jogos do mandante e visitante, permitindo avaliar a consistência e variação de seus desempenhos. Uma visão completa das médias individuais e totais dos times é proporcionada para uma análise rápida e eficaz.',
                            style: TextStyle(
                              color: AppColors.secundaryColor.withOpacity(.8),
                              fontFamily: 'Roboto',
                              fontStyle: FontStyle.italic,
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    ),
                    icon: const Icon(Icons.info),
                    color: AppColors.secundaryColor,
                  ),
                  const SizedBox(width: 10),
                ],
              ),
              SfCartesianChart(
                selectionType: SelectionType.point,
                tooltipBehavior: TooltipBehavior(enable: true),
                primaryYAxis: NumericAxis(),
                legend: Legend(
                  textStyle: const TextStyle(
                      fontFamily: 'Roboto', color: Colors.white),
                  position: LegendPosition.bottom,
                  isVisible: true,
                ),
                series:
                    _getSeries(controller.match.home, controller.match.away),
              ),
            ],
          ),
        );
      },
    );
  }

  List<SplineAreaSeries<ChartData, num>> _getSeries(String home, String away) {
    return [
      SplineAreaSeries<ChartData, num>(
        enableTooltip: true,
        color: Colors.amber.withOpacity(0.2),
        borderColor: Colors.amber,
        borderWidth: 2,
        dataSource: controller.chartData,
        xValueMapper: (ChartData sales, _) => sales.index,
        yValueMapper: (ChartData sales, _) => sales.total.toDouble(),
        markerSettings: const MarkerSettings(
          color: Colors.amber,
          isVisible: true,
          height: 3.5,
          width: 3.5,
        ),
        name: 'Total da Partida',
      ),
      SplineAreaSeries<ChartData, num>(
        color: Colors.pink.withOpacity(0.2),
        borderColor: Colors.pink,
        borderWidth: 2,
        dataSource: controller.chartData,
        xValueMapper: (ChartData sales, _) => sales.index,
        yValueMapper: (ChartData sales, _) => sales.home.toDouble(),
        markerSettings: const MarkerSettings(
          color: Colors.pink,
          isVisible: true,
          height: 3.5,
          width: 3.5,
        ),
        name: home,
      ),
      SplineAreaSeries<ChartData, num>(
        color: Colors.blue.withOpacity(0.2),
        borderColor: Colors.blue,
        borderWidth: 2,
        dataSource: controller.chartData,
        xValueMapper: (ChartData sales, _) => sales.index,
        yValueMapper: (ChartData sales, _) => sales.away.toDouble(),
        markerSettings: const MarkerSettings(
          color: Colors.blue,
          isVisible: true,
          height: 3.5,
          width: 3.5,
        ),
        name: away,
      ),
    ];
  }
}
