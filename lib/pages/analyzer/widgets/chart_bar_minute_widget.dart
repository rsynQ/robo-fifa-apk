import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/models/chart/chart_minute.dart';
import 'package:robo_fifa/pages/analyzer/controller/analyzer_controller.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../core/ui/app_colors.dart';
import '../../../core/ui/dialog/info_dialog.dart';

class ChartBarMinuteWidget extends StatelessWidget {
  const ChartBarMinuteWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AnalyzerController>(
      builder: (controller) {
        return Card(
          surfaceTintColor: AppColors.primaryColor,
          color: AppColors.primaryColor,
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  const SizedBox(width: 10),
                  Text(
                      'Média de Gols por minuto últ. ${(controller.chartMinute.isNotEmpty ? controller.analyzer.home.matches : 0)} jogos',
                      style: TextStyle(
                          color: AppColors.secundaryColor,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold,
                          fontSize: 14)),
                  const Spacer(),
                  IconButton(
                    onPressed: () => Get.dialog(
                      InfoDialog(
                        title: 'IMPORTANTE',
                        children: [
                          Row(
                            children: [
                              Container(
                                  width: 20, height: 20, color: Colors.amber),
                              const SizedBox(width: 10),
                              Text(
                                'H2H - Médias Confrontos Direto',
                                style: TextStyle(
                                  color:
                                      AppColors.secundaryColor.withOpacity(.8),
                                  fontFamily: 'Roboto',
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Container(
                                  width: 20, height: 20, color: Colors.pink),
                              const SizedBox(width: 10),
                              Text(
                                'MÉDIAS DO MANDANTE',
                                style: TextStyle(
                                  color:
                                      AppColors.secundaryColor.withOpacity(.8),
                                  fontFamily: 'Roboto',
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Container(
                                  width: 20, height: 20, color: Colors.blue),
                              const SizedBox(width: 10),
                              Text(
                                'MÉDIAS DO VISITANTE',
                                style: TextStyle(
                                  color:
                                      AppColors.secundaryColor.withOpacity(.8),
                                  fontFamily: 'Roboto',
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 20),
                          Text(
                            'O gráfico mostra as médias de gols marcados por minuto, auxiliando na determinação do momento ideal para realizar a entrada.',
                            style: TextStyle(
                              color: AppColors.secundaryColor.withOpacity(.8),
                              fontFamily: 'Roboto',
                              fontStyle: FontStyle.italic,
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    ),
                    icon: const Icon(Icons.info),
                    color: AppColors.secundaryColor,
                  ),
                  const SizedBox(width: 10),
                ],
              ),
              SfCartesianChart(
                tooltipBehavior: TooltipBehavior(enable: true),
                legend: Legend(
                  textStyle: const TextStyle(
                      fontFamily: 'Roboto', color: Colors.white),
                  position: LegendPosition.bottom,
                  isVisible: true,
                ),
                primaryXAxis: CategoryAxis(
                  majorGridLines: const MajorGridLines(width: 0),
                ),
                primaryYAxis: NumericAxis(
                  name: 'primaryAxis',
                ),
                axes: <ChartAxis>[
                  NumericAxis(
                    name: 'secondaryAxis',
                  ),
                  NumericAxis(
                    name: 'trirdAxis',
                  )
                ],
                series: <ChartSeries>[
                  ColumnSeries<ChartMinute, String>(
                      dataSource: controller.chartMinute,
                      xValueMapper: (ChartMinute data, _) => data.x,
                      yValueMapper: (ChartMinute data, _) => data.a,
                      yAxisName: 'primaryAxis',
                      name: 'H2H',
                      color: Colors.amber),
                  ColumnSeries<ChartMinute, String>(
                      dataSource: controller.chartMinute,
                      xValueMapper: (ChartMinute data, _) => data.x,
                      yValueMapper: (ChartMinute data, _) => data.z,
                      yAxisName: 'primaryAxis',
                      name: controller.match.home,
                      color: Colors.pink),
                  ColumnSeries<ChartMinute, String>(
                    dataSource: controller.chartMinute,
                    xValueMapper: (ChartMinute data, _) => data.x,
                    yValueMapper: (ChartMinute data, _) => data.y,
                    yAxisName: 'primaryAxis',
                    name: controller.match.away,
                    color: Colors.blue,
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
