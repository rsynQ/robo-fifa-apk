import 'package:flutter/material.dart';

import '../../../core/ui/app_colors.dart';

class PercentWidget extends StatelessWidget {
  final String label;
  final String percent;
  const PercentWidget(this.label, this.percent, {super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(label,
            style: TextStyle(
                color: AppColors.secundaryColor,
                fontFamily: 'Roboto',
                fontSize: 14,
                fontWeight: FontWeight.bold)),
        Text(
          '$percent%',
          style: TextStyle(
              color: AppColors.secundaryColor,
              fontFamily: 'Roboto',
              fontSize: 20,
              fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
