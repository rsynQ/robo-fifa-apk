import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../core/ui/app_colors.dart';
import '../../../core/ui/dialog/info_dialog.dart';
import '../../../models/chart/pie_data.dart';
import '../controller/analyzer_controller.dart';

class SingleCircularWidget extends StatelessWidget {
  final String label;
  final List<PieData> pieData;
  const SingleCircularWidget(
      {super.key, required this.pieData, required this.label});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AnalyzerController>(
      builder: (controller) {
        return Card(
          surfaceTintColor: AppColors.primaryColor,
          color: AppColors.primaryColor,
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  const SizedBox(width: 10),
                  Text('Desempenho com $label',
                      style: TextStyle(
                          color: AppColors.secundaryColor,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold,
                          fontSize: 14)),
                  const Spacer(),
                  IconButton(
                    onPressed: () => Get.dialog(
                      InfoDialog(
                        title: 'IMPORTANTE',
                        children: [
                          Text(
                            'O gráfico ilustra o desempenho do jogador ao longo dos últimos 20 jogos com esta equipe.',
                            style: TextStyle(
                              color: AppColors.secundaryColor.withOpacity(.8),
                              fontFamily: 'Roboto',
                              fontStyle: FontStyle.italic,
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    ),
                    icon: const Icon(Icons.info),
                    color: AppColors.secundaryColor,
                  ),
                  const SizedBox(width: 10),
                ],
              ),
              SizedBox(
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: SfCircularChart(
                        legend: Legend(
                            isVisible: true,
                            textStyle: const TextStyle(
                                color: Colors.grey,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold)),
                        margin: const EdgeInsets.all(0),
                        series: <PieSeries<PieData, String>>[
                          PieSeries<PieData, String>(
                            explode: true,
                            explodeIndex: 0,
                            dataSource: pieData,
                            xValueMapper: (PieData data, _) => data.xData,
                            yValueMapper: (PieData data, _) => data.yData,
                            dataLabelMapper: (PieData data, _) => data.text,
                            dataLabelSettings:
                                const DataLabelSettings(isVisible: true),
                            pointColorMapper: (PieData data, _) => data.color,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
