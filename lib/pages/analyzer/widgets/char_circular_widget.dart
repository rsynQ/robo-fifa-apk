import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/pages/analyzer/controller/analyzer_controller.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../core/ui/app_colors.dart';
import '../../../core/ui/dialog/info_dialog.dart';
import '../../../models/chart/pie_data.dart';

class CharCircularWidget extends StatelessWidget {
  const CharCircularWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AnalyzerController>(
      builder: (controller) {
        return Card(
          surfaceTintColor: AppColors.primaryColor,
          color: AppColors.primaryColor,
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  const SizedBox(width: 10),
                  Text(
                      'Desempenho últ. ${(controller.chartMinute.isNotEmpty ? controller.analyzer.home.matches : 0)} jogos',
                      style: TextStyle(
                          color: AppColors.secundaryColor,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold,
                          fontSize: 14)),
                  const Spacer(),
                  IconButton(
                    onPressed: () => Get.dialog(InfoDialog(
                      title: 'IMPORTANTE',
                      children: [
                        Row(
                          children: [
                            Container(
                                width: 20, height: 20, color: Colors.pink),
                            const SizedBox(width: 10),
                            Text(
                              'MANDANTE',
                              style: TextStyle(
                                color: AppColors.secundaryColor.withOpacity(.8),
                                fontFamily: 'Roboto',
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            Container(
                                width: 20, height: 20, color: Colors.amber),
                            const SizedBox(width: 10),
                            Text(
                              'H2H - Confrontos Direto',
                              style: TextStyle(
                                color: AppColors.secundaryColor.withOpacity(.8),
                                fontFamily: 'Roboto',
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            Container(
                                width: 20, height: 20, color: Colors.blue),
                            const SizedBox(width: 10),
                            Text(
                              'VISITANTE',
                              style: TextStyle(
                                color: AppColors.secundaryColor.withOpacity(.8),
                                fontFamily: 'Roboto',
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 20),
                        Text(
                          'O gráfico apresenta o desempenho de ambos os jogadores tanto em partidas individuais quanto em confrontos diretos.',
                          style: TextStyle(
                            color: AppColors.secundaryColor.withOpacity(.8),
                            fontFamily: 'Roboto',
                            fontStyle: FontStyle.italic,
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    )),
                    icon: const Icon(Icons.info),
                    color: AppColors.secundaryColor,
                  ),
                  const SizedBox(width: 10),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    controller.match.home,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.pink,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                  const Text(
                    'H2H',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.amber,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                  Text(
                    controller.match.away,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: SfCircularChart(
                        margin: const EdgeInsets.all(0),
                        legend: Legend(
                          isVisible: false,
                          position: LegendPosition.bottom,
                          legendItemBuilder:
                              (legendText, series, point, seriesIndex) {
                            if (seriesIndex == 1) {
                              return Text(
                                controller.match.home,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontFamily: 'Roboto',
                                  color: Colors.pink,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10,
                                ),
                              );
                            } else {
                              return const SizedBox(width: 25);
                            }
                          },
                        ),
                        series: <PieSeries<PieData, String>>[
                          PieSeries<PieData, String>(
                            explode: true,
                            explodeIndex: 0,
                            dataSource: controller.pieDataHome,
                            xValueMapper: (PieData data, _) => data.xData,
                            yValueMapper: (PieData data, _) => data.yData,
                            dataLabelMapper: (PieData data, _) => data.text,
                            dataLabelSettings:
                                const DataLabelSettings(isVisible: true),
                            pointColorMapper: (PieData data, _) => data.color,
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: controller.pieDataH2H.isNotEmpty,
                      child: Expanded(
                        child: SfCircularChart(
                          legend: Legend(
                            isVisible: false,
                            position: LegendPosition.bottom,
                            legendItemBuilder:
                                (legendText, series, point, seriesIndex) {
                              if (seriesIndex == 2) {
                                return const Text(
                                  'H2H',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.amber,
                                    fontSize: 10,
                                  ),
                                );
                              } else {
                                return const SizedBox(width: 30);
                              }
                            },
                          ),
                          margin: const EdgeInsets.all(0),
                          series: <PieSeries<PieData, String>>[
                            PieSeries<PieData, String>(
                              explode: true,
                              explodeIndex: 0,
                              dataSource: controller.pieDataH2H,
                              xValueMapper: (PieData data, _) => data.xData,
                              yValueMapper: (PieData data, _) => data.yData,
                              dataLabelMapper: (PieData data, _) => data.text,
                              dataLabelSettings:
                                  const DataLabelSettings(isVisible: true),
                              pointColorMapper: (PieData data, _) => data.color,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: SfCircularChart(
                        legend: Legend(
                          isVisible: false,
                          position: LegendPosition.bottom,
                          legendItemBuilder:
                              (legendText, series, point, seriesIndex) {
                            if (seriesIndex == 1) {
                              return Text(
                                controller.match.away,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                  fontSize: 10,
                                ),
                              );
                            } else {
                              return const SizedBox(width: 25);
                            }
                          },
                        ),
                        margin: const EdgeInsets.all(0),
                        series: <PieSeries<PieData, String>>[
                          PieSeries<PieData, String>(
                            explode: true,
                            explodeIndex: 0,
                            dataSource: controller.pieDataAway,
                            xValueMapper: (PieData data, _) => data.xData,
                            yValueMapper: (PieData data, _) => data.yData,
                            dataLabelMapper: (PieData data, _) => data.text,
                            dataLabelSettings:
                                const DataLabelSettings(isVisible: true),
                            pointColorMapper: (PieData data, _) => data.color,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
