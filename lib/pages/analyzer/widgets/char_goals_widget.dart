import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../core/ui/app_colors.dart';
import '../../../core/ui/dialog/info_dialog.dart';
import '../../../models/chart/chart_data.dart';
import '../controller/analyzer_controller.dart';

class CharGoalsWidget extends GetView<AnalyzerController> {
  final List<ChartData> chartData;
  const CharGoalsWidget({super.key, required this.chartData});

  @override
  Widget build(BuildContext context) {
    return Card(
      surfaceTintColor: AppColors.primaryColor,
      color: AppColors.primaryColor,
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        children: [
          Row(
            children: [
              const SizedBox(width: 10),
              Text('Gols (Marcados/Concedidos)',
                  style: TextStyle(
                      color: AppColors.secundaryColor,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.bold,
                      fontSize: 14)),
              const Spacer(),
              IconButton(
                onPressed: () => Get.dialog(
                  InfoDialog(
                    title: 'IMPORTANTE',
                    children: [
                      Text(
                        'O gráfico oferece uma visão do momento do jogador, destacando tanto sua capacidade de marcar gols como de sofrer.',
                        style: TextStyle(
                          color: AppColors.secundaryColor.withOpacity(.8),
                          fontFamily: 'Roboto',
                          fontStyle: FontStyle.italic,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
                icon: const Icon(Icons.info),
                color: AppColors.secundaryColor,
              ),
              const SizedBox(width: 10),
            ],
          ),
          SfCartesianChart(
            selectionType: SelectionType.point,
            tooltipBehavior: TooltipBehavior(enable: true),
            primaryYAxis: NumericAxis(),
            legend: Legend(
              textStyle:
                  const TextStyle(fontFamily: 'Roboto', color: Colors.white),
              position: LegendPosition.bottom,
              isVisible: true,
            ),
            series: _getSeries(
                chartData, controller.match.home, controller.match.away),
          ),
        ],
      ),
    );
  }

  List<SplineAreaSeries<ChartData, num>> _getSeries(
      List<ChartData> chartData, String home, String away) {
    return [
      SplineAreaSeries<ChartData, num>(
        color: Colors.pink.withOpacity(0.2),
        borderColor: Colors.pink,
        borderWidth: 2,
        dataSource: chartData,
        xValueMapper: (ChartData sales, _) => sales.index,
        yValueMapper: (ChartData sales, _) => sales.home.toDouble(),
        markerSettings: const MarkerSettings(
          color: Colors.pink,
          isVisible: true,
          height: 3.5,
          width: 3.5,
        ),
        name: 'Marcados',
      ),
      SplineAreaSeries<ChartData, num>(
        color: Colors.blue.withOpacity(0.2),
        borderColor: Colors.blue,
        borderWidth: 2,
        dataSource: chartData,
        xValueMapper: (ChartData sales, _) => sales.index,
        yValueMapper: (ChartData sales, _) => sales.away.toDouble(),
        markerSettings: const MarkerSettings(
          color: Colors.blue,
          isVisible: true,
          height: 3.5,
          width: 3.5,
        ),
        name: 'Concedidos',
      ),
    ];
  }
}
