import '../../../core/constants/endpoints.dart';
import '../../../core/http/http_manager.dart';
import '../../../models/stats/analyzer_model.dart';
import '../result/analyzer_result.dart';

class AnalyzerRepository {
  final HttpManager _http = HttpManager();

  Future<AnalyzerResult> getMatchDetails(int matchID) async {
    final response = await _http.restRequest(
        url: Endpoints.showMatch,
        method: HttpMethods.get,
        body: {'id': matchID});

    if (response['result'] != null) {
      final data = AnalyzerModel.fromMap(response['result']['analyzer']);
      return AnalyzerResult.success(data);
    } else {
      return AnalyzerResult.error(response['error']);
    }
  }
}
