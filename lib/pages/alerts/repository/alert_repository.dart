import '../../../core/constants/endpoints.dart';
import '../../../core/helpers/secure_storage.dart';
import '../../../core/http/http_manager.dart';
import '../../../core/result/data_result.dart';
import '../../../models/reports/report_model.dart';

class AlertRepository {
  final HttpManager _http = HttpManager();

  Future<DataResult<ReportModel>> getAlerts() async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    final response = await _http.restRequest(
      url: Endpoints.alerts,
      method: HttpMethods.get,
      headers: {'Authorization': 'Bearer $token'},
    );

    if (response['result'] != null) {
      List<ReportModel> data =
          (List<Map<String, dynamic>>.from(response['result']))
              .map(ReportModel.fromMap)
              .toList();

      return DataResult.success(data);
    } else {
      return DataResult.error(response['error']);
    }
  }
}
