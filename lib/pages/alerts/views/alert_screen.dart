import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/components/custom_app_bar.dart';
import 'package:robo_fifa/core/helpers/date_helper.dart';
import 'package:robo_fifa/core/helpers/launch_helper.dart';
import 'package:robo_fifa/pages/alerts/controller/alert_controller.dart';

import '../../../core/ui/app_colors.dart';

class AlertScreen extends StatelessWidget {
  const AlertScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: GetBuilder<AlertController>(
        builder: (controller) {
          return Column(
            children: [
              Expanded(
                child: RefreshIndicator(
                  onRefresh: controller.getAlerts,
                  child: ListView.builder(
                    itemCount: controller.reports.length,
                    itemBuilder: (context, index) {
                      final report = controller.reports[index];
                      return InkWell(
                        onTap: () {},
                        child: Card(
                          surfaceTintColor: AppColors.primaryColor,
                          color: AppColors.primaryColor,
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'BOT: ${report.bot_name}',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: AppColors.textColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      DateHelper.formatDateTimeUTC(
                                          report.created_at),
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: AppColors.secundaryColor
                                              .withOpacity(0.8),
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text(
                                      '${report.home_name} (${report.home_team})',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: AppColors.secundaryColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w500),
                                    ),
                                    const Spacer(),
                                    Text(
                                      '${report.status < 0 ? 'Placar ${report.score}' : 'Placar ${report.home_goals_ft}:${report.away_goals_ft}'}  Odd: ${(report.line_odd).toStringAsFixed(2)}  Linha: ${report.handicap}',
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: AppColors.secundaryColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text(
                                      '${report.away_name} (${report.away_team})',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: AppColors.secundaryColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w500),
                                    ),
                                    const Spacer(),
                                    Visibility(
                                      visible: report.status >= 0,
                                      child: Badge(
                                        largeSize: 20.0,
                                        backgroundColor: report.roi > 0
                                            ? Colors.green
                                            : Colors.red,
                                        label: Text(
                                            'ROI: ${(report.roi).toStringAsFixed(2)}'),
                                      ),
                                    ),
                                    Visibility(
                                      visible: report.status < 0,
                                      child: const Badge(
                                        largeSize: 20.0,
                                        backgroundColor: Colors.amber,
                                        label: Text(
                                          'EM ANDAMENTO',
                                          style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Visibility(
                                  visible: report.status < 0,
                                  child: Container(
                                    height: 30,
                                    color: AppColors.greyColor,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        TextButton(
                                            onPressed: () async =>
                                                await LaunchHelper.getLaunchUrl(
                                                    Uri.parse(
                                                        'https://www.bet365.com/#/AX/K^E-soccer%20-%20Battle')),
                                            child: Text(
                                              'ABRIR NA BET365',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: AppColors.textColor,
                                                  fontSize: 10,
                                                  fontFamily: 'Roboto'),
                                            ))
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
