import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/pages/alerts/repository/alert_repository.dart';

import '../../../models/reports/report_model.dart';

class AlertController extends GetxController {
  final repository = AlertRepository();

  final List<ReportModel> _reports = [];

  List<ReportModel> get reports => _reports;

  @override
  void onInit() async {
    await getAlerts();
    super.onInit();
  }

  Future<void> getAlerts() async {
    EasyLoading.show();
    final result = await repository.getAlerts();
    result.when(
      success: (data) {
        _reports.assignAll(data);
        EasyLoading.dismiss();
        update();
      },
      error: (message) {
        EasyLoading.showError(message);
      },
    );
  }
}
