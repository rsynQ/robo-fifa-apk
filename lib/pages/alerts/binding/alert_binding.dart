import 'package:get/get.dart';
import 'package:robo_fifa/pages/alerts/controller/alert_controller.dart';

class AlertBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(AlertController());
  }
}
