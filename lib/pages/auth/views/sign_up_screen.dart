import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/models/user/user_model.dart';

import '../../../core/auth/controller/auth_controller.dart';
import '../../../core/helpers/validators.dart';
import '../../../core/ui/app_colors.dart';
import '../../../core/ui/forms/custom_text_form_field.dart';
import '../../../routes/pages_route.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  final controller = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.greyColor,
      body: SingleChildScrollView(
        child: SizedBox(
          height: size.height,
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Card(
                  surfaceTintColor: AppColors.primaryColor,
                  color: AppColors.primaryColor,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Image.asset(
                          'assets/logo/icon.png',
                          width: 50,
                        ),
                      ),
                      CustomTextFormField(
                        label: 'Como gostaria de ser chamado?',
                        controller: nameController,
                        validator: nameValidator,
                        keyboardType: TextInputType.name,
                        maxLength: 20,
                      ),
                      CustomTextFormField(
                        label: 'Preciso do seu e-mail ?',
                        controller: emailController,
                        validator: emailValidator,
                        keyboardType: TextInputType.emailAddress,
                      ),
                      CustomTextFormField(
                        label:
                            'Por favor, informe um número de telefone para contato.',
                        controller: phoneController,
                        validator: phoneValidator,
                        keyboardType: TextInputType.phone,
                      ),
                      CustomTextFormField(
                          label: 'Por favor, escolha uma senha para acessar.',
                          controller: passwordController,
                          validator: passwordValidator,
                          isSecret: true),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () => Get.back(),
                              child: Text('VOLTAR',
                                  style: TextStyle(
                                      color: AppColors.secundaryColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14))),
                          const SizedBox(width: 20),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: AppColors.textColor,
                            ),
                            onPressed: () async {
                              if (formKey.currentState!.validate()) {
                                String username = nameController.text;
                                String email = emailController.text;
                                String password = passwordController.text;
                                String phone = phoneController.text;
                                await controller.createAccount(UserModel(
                                    username: username,
                                    email: email,
                                    password: password,
                                    phone: phone,
                                    register_type: 'APP'));
                              }
                            },
                            icon: Icon(
                              Icons.login,
                              color: AppColors.textBlack,
                            ),
                            label: Text('CRIAR CONTA',
                                style: TextStyle(
                                    color: AppColors.textBlack,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            Expanded(
                              child: Divider(
                                color: Colors.grey.withAlpha(90),
                                thickness: 2,
                              ),
                            ),
                            const SizedBox(height: 10),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              child: Text('OU',
                                  style: TextStyle(
                                      color: AppColors.greyColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ),
                            Expanded(
                              child: Divider(
                                color: Colors.grey.withAlpha(90),
                                thickness: 2,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                              onPressed: () =>
                                  Get.toNamed(PagesRoute.signUpRoute),
                              child: Text('CRIAR UMA CONTA',
                                  style: TextStyle(
                                      color: AppColors.secundaryColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10))),
                          TextButton(
                              onPressed: () => Get.back(),
                              child: Text('RECUPERAR CONTA',
                                  style: TextStyle(
                                      color: AppColors.secundaryColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10))),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
