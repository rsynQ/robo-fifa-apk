import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/auth/controller/auth_controller.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';
import 'package:robo_fifa/core/ui/forms/custom_text_form_field.dart';
import 'package:robo_fifa/routes/pages_route.dart';

import '../../../core/helpers/validators.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final controller = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.greyColor,
      body: SingleChildScrollView(
        child: SizedBox(
          height: size.height,
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Card(
                  surfaceTintColor: AppColors.primaryColor,
                  color: AppColors.primaryColor,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Image.asset(
                          'assets/logo/icon.png',
                          width: 50,
                        ),
                      ),
                      CustomTextFormField(
                        label: 'Seu endereço de e-mail.',
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        validator: emailValidator,
                      ),
                      CustomTextFormField(
                        label: 'Insira sua senha.',
                        isSecret: true,
                        controller: passwordController,
                        validator: passwordValidator,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () => Get.back(),
                              child: Text('VOLTAR',
                                  style: TextStyle(
                                      color: AppColors.secundaryColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14))),
                          const SizedBox(width: 20),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: AppColors.textColor,
                            ),
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                String email = emailController.text;
                                String password = passwordController.text;
                                controller.login(email, password);
                              }
                            },
                            icon: Icon(
                              Icons.login,
                              color: AppColors.textBlack,
                            ),
                            label: Text('ENTRAR',
                                style: TextStyle(
                                    color: AppColors.textBlack,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            Expanded(
                              child: Divider(
                                color: Colors.grey.withAlpha(90),
                                thickness: 2,
                              ),
                            ),
                            const SizedBox(height: 10),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              child: Text('OU',
                                  style: TextStyle(
                                      color: AppColors.greyColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ),
                            Expanded(
                              child: Divider(
                                color: Colors.grey.withAlpha(90),
                                thickness: 2,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                              onPressed: () =>
                                  Get.toNamed(PagesRoute.signUpRoute),
                              child: Text('CRIAR UMA CONTA',
                                  style: TextStyle(
                                      color: AppColors.secundaryColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10))),
                          TextButton(
                              onPressed: () => Get.back(),
                              child: Text('RECUPERAR CONTA',
                                  style: TextStyle(
                                      color: AppColors.secundaryColor,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10))),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
