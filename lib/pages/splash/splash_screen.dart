import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSplashScreen(
        duration: 2500,
        splash: Center(
          child: Image.asset('assets/logo/icon.png'),
        ),
        nextScreen: const SplashScreen(),
        centered: true,
        disableNavigation: true,
        splashTransition: SplashTransition.scaleTransition,
        animationDuration: const Duration(milliseconds: 2500),
        curve: Curves.bounceOut,
        backgroundColor: AppColors.primaryColor,
      ),
    );
  }
}
