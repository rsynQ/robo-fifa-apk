import 'package:get/get.dart';
import 'package:robo_fifa/pages/welcome/controller/welcome_controller.dart';

class WelcomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(WelcomeController());
  }
}
