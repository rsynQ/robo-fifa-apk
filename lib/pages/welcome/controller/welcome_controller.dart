import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:robo_fifa/core/helpers/admob_helper.dart';

import '../../../core/auth/controller/auth_controller.dart';
import '../../../core/constants/storage_keys.dart';
import '../../../core/helpers/ranking_helper.dart';
import '../../../core/helpers/secure_storage.dart';
import '../../../models/ranking/ranking_model.dart';

class WelcomeController extends GetxController {
  final auth = Get.find<AuthController>();
  RxBool ishours = false.obs;
  double maxium = 100;
  int option = 0;
  InterstitialAd? interstitialAd;
  final RxList<RankingModel> _highlights = RxList([]);

  List<RankingModel> get highlights => _highlights;

  @override
  void onInit() async {
    loadHighlightsList();
    createInterstitialAd();
    await getMe();
    super.onInit();
  }

  void optionMakerd(int option) {
    this.option = option;
    update();
  }

  Future<void> getMe() async {
    auth.user = null;
    final token =
        await SecureStorage.getLocalData(key: StorageKeys.accessToken);

    if (token != null) {
      await auth.me();
    }
    update();
  }

  Future<void> onRefresh() async {
    await auth.getMatches();
    loadHighlightsList();
    ishours.value = false;
    update();
  }

  void loadHighlightsList() {
    _highlights.clear();

    auth.rankings.sort((a, b) {
      return RankingHelper.customSortFunction(a, b);
    });

    int count = 0;
    for (var ranking in auth.rankings) {
      if (ranking.ap >= 50) {
        _highlights.add(ranking);
        count++;
      }
      if (count == 5 || count == auth.rankings.length) break;
    }

    auth.rankings.sort((a, b) => b.p.compareTo(a.p));
  }

  void createInterstitialAd() {
    InterstitialAd.load(
        adUnitId: AdmobHelper.InterstitialAdUnitId,
        request: const AdRequest(),
        adLoadCallback:
            InterstitialAdLoadCallback(onAdLoaded: (InterstitialAd ad) {
          interstitialAd = ad;
          interstitialAd!.setImmersiveMode(true);
        }, onAdFailedToLoad: (LoadAdError err) {
          interstitialAd = null;
        }));
  }

  void showInterstitialAd() {
    if (interstitialAd == null) {
      return;
    }
    interstitialAd!.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (InterstitialAd ad) =>
          // ignore: avoid_print
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (InterstitialAd ad) {
        ad.dispose();
        createInterstitialAd();
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        ad.dispose();
        createInterstitialAd();
      },
    );
    interstitialAd!.show();
    interstitialAd = null;
  }

  void updateRaking() {
    auth.treatData(ishours.value);
    loadHighlightsList();
    update();
  }
}
