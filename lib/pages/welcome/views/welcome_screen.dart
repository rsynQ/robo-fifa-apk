import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/toggle/gf_toggle.dart';
import 'package:getwidget/types/gf_toggle_type.dart';
import 'package:robo_fifa/core/drawer/drawer_gf.dart';
import 'package:robo_fifa/core/helpers/date_helper.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';
import 'package:robo_fifa/routes/pages_route.dart';

import '../../../core/components/custom_app_bar.dart';
import '../../../core/components/custom_banner.dart';
import '../controller/welcome_controller.dart';

class WelcomeScreen extends GetView<WelcomeController> {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const CustomAppBar(),
        drawer: const DrawerGf(),
        body: GetBuilder<WelcomeController>(
          builder: (controller) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(width: 10),
                      const Text(
                        'Destaques do momento',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      ),
                      const Spacer(),
                      const Text(
                        'Últimas 2h',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(width: 5),
                      GFToggle(
                        onChanged: (val) {
                          controller.ishours.value = val!;
                          controller.updateRaking();
                        },
                        value: controller.ishours.value,
                        type: GFToggleType.ios,
                        enabledTrackColor: AppColors.textColor,
                        enabledThumbColor: AppColors.primaryColor,
                      ),
                      const SizedBox(width: 5),
                    ],
                  ),
                  SizedBox(
                    height: 120,
                    width: double.infinity,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: controller.highlights.length,
                      itemBuilder: (context, index) {
                        final high = controller.highlights[index];
                        return SizedBox(
                          width: 200,
                          child: Card(
                            surfaceTintColor: AppColors.primaryColor,
                            color: AppColors.primaryColor,
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                            shadowColor: Colors.grey,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(high.name,
                                      style: TextStyle(
                                          color: AppColors.textColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.bold)),
                                  Divider(color: AppColors.textColor),
                                  Text('${high.v} vitórias',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontStyle: FontStyle.italic,
                                          color: AppColors.secundaryColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w400)),
                                  Text('${high.j} partidas jogadas',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontStyle: FontStyle.italic,
                                          color: AppColors.secundaryColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Visibility(
                    visible: !controller.auth.isLogged(),
                    child: Card(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: const Offset(0, 3),
                            ),
                          ],
                        ),
                        child: InkWell(
                          onTap: () => Get.toNamed(PagesRoute.signInRoute),
                          child: Container(
                            color: AppColors.textColor,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                TextButton.icon(
                                  onPressed: () =>
                                      Get.toNamed(PagesRoute.signInRoute),
                                  icon: Icon(Icons.login_outlined,
                                      color: AppColors.textBlack),
                                  label: Text(
                                    'Acesse sua conta agora',
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.textBlack,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                      visible: !controller.auth.isPRO(),
                      child: const CustomBanner()),
                  RefreshIndicator(
                    onRefresh: controller.onRefresh,
                    child: Card(
                      surfaceTintColor: AppColors.primaryColor,
                      color: AppColors.primaryColor,
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      shadowColor: Colors.grey,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                onPressed: () => controller.optionMakerd(0),
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: controller.option == 0
                                      ? AppColors.secundaryColor
                                          .withOpacity(0.2)
                                      : AppColors.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                child: Text('Próximas partidas',
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: AppColors.textColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.bold)),
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: controller.option == 1
                                      ? AppColors.secundaryColor
                                          .withOpacity(0.2)
                                      : AppColors.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                onPressed: () => controller.optionMakerd(1),
                                child: Text('Últimas partidas',
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: AppColors.textColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.bold)),
                              ),
                            ],
                          ),
                          Visibility(
                            visible: controller.option == 0,
                            child: SizedBox(
                              height: 300,
                              child: ListView.builder(
                                itemCount: controller.auth.matchesOpen.length,
                                itemBuilder: (context, index) {
                                  final match =
                                      controller.auth.matchesOpen[index];
                                  return InkWell(
                                    onTap: () {
                                      if (!controller.auth.isPRO()) {
                                        controller.showInterstitialAd();
                                      }

                                      Get.toNamed(PagesRoute.analyzerRoute,
                                          arguments: match);
                                    },
                                    child: Card(
                                      color: AppColors.greyColor,
                                      elevation: 1,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(12),
                                      ),
                                      shadowColor: AppColors.textColor,
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: 75,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    DateHelper.formatDateTime(
                                                      int.parse(match.datetime),
                                                      format: 'dd/MM/yyyy',
                                                    ),
                                                    style: TextStyle(
                                                      fontSize: 10,
                                                      color: AppColors
                                                          .secundaryColor,
                                                      fontFamily: 'Roboto',
                                                    ),
                                                  ),
                                                  Text(
                                                    DateHelper.formatDateTime(
                                                      int.parse(match.datetime),
                                                      format: 'HH:mm',
                                                    ),
                                                    style: TextStyle(
                                                      fontSize: 10,
                                                      color: AppColors
                                                          .secundaryColor,
                                                      fontFamily: 'Roboto',
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              width: 130,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    match.home,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 12,
                                                      color: AppColors
                                                          .secundaryColor,
                                                      fontFamily: 'Roboto',
                                                    ),
                                                  ),
                                                  Text(
                                                    match.home_team,
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      color: AppColors
                                                          .secundaryColor,
                                                      fontFamily: 'Roboto',
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(width: 5),
                                            Text(
                                              ' x ',
                                              style: TextStyle(
                                                fontSize: 11,
                                                color: AppColors.secundaryColor,
                                                fontFamily: 'Roboto',
                                              ),
                                            ),
                                            const SizedBox(width: 5),
                                            SizedBox(
                                              width: 130,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    match.away,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 12,
                                                      color: AppColors
                                                          .secundaryColor,
                                                      fontFamily: 'Roboto',
                                                    ),
                                                  ),
                                                  Text(
                                                    match.away_team,
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      color: AppColors
                                                          .secundaryColor,
                                                      fontFamily: 'Roboto',
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          Visibility(
                            visible: controller.option == 1,
                            child: SizedBox(
                              height: 300,
                              child: ListView.builder(
                                itemCount:
                                    controller.auth.matchesClosed.length > 20
                                        ? 20
                                        : controller.auth.matchesClosed.length,
                                itemBuilder: (context, index) {
                                  final match =
                                      controller.auth.matchesClosed[index];
                                  return Card(
                                    color: AppColors.greyColor,
                                    elevation: 1,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    shadowColor: AppColors.textColor,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 75,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  DateHelper.formatDateTime(
                                                    int.parse(match.datetime),
                                                    format: 'dd/MM/yyyy',
                                                  ),
                                                  style: TextStyle(
                                                    fontSize: 10,
                                                    color: AppColors
                                                        .secundaryColor,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                ),
                                                Text(
                                                  DateHelper.formatDateTime(
                                                    int.parse(match.datetime),
                                                    format: 'HH:mm',
                                                  ),
                                                  style: TextStyle(
                                                    fontSize: 10,
                                                    color: AppColors
                                                        .secundaryColor,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            width: 120,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  match.home,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12,
                                                    color: AppColors
                                                        .secundaryColor,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                ),
                                                Text(
                                                  match.home_team,
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    color: AppColors
                                                        .secundaryColor,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          const SizedBox(width: 5),
                                          Column(
                                            children: [
                                              Text(
                                                '(${match.home_goals_ht})',
                                                style: TextStyle(
                                                  fontSize: 9,
                                                  color:
                                                      AppColors.secundaryColor,
                                                  fontFamily: 'Roboto',
                                                ),
                                              ),
                                              Text(
                                                '${match.home_goals_ft}',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 10,
                                                  color:
                                                      AppColors.secundaryColor,
                                                  fontFamily: 'Roboto',
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            ' x ',
                                            style: TextStyle(
                                              fontSize: 11,
                                              color: AppColors.secundaryColor,
                                              fontFamily: 'Roboto',
                                            ),
                                          ),
                                          Column(
                                            children: [
                                              Text(
                                                '(${match.away_goals_ht})',
                                                style: TextStyle(
                                                  fontSize: 9,
                                                  color:
                                                      AppColors.secundaryColor,
                                                  fontFamily: 'Roboto',
                                                ),
                                              ),
                                              Text(
                                                '${match.away_goals_ft}',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 10,
                                                  color:
                                                      AppColors.secundaryColor,
                                                  fontFamily: 'Roboto',
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(width: 5),
                                          SizedBox(
                                            width: 120,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  match.away,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12,
                                                    color: AppColors
                                                        .secundaryColor,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                ),
                                                Text(
                                                  match.away_team,
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    color: AppColors
                                                        .secundaryColor,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    surfaceTintColor: AppColors.primaryColor,
                    color: AppColors.primaryColor,
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    shadowColor: Colors.grey,
                    child: DataTable(
                      columnSpacing: 5,
                      columns: [
                        DataColumn(
                          label: Text('Player',
                              style:
                                  TextStyle(color: AppColors.secundaryColor)),
                        ),
                        DataColumn(
                            label: Text(
                              'J',
                              style: TextStyle(color: AppColors.secundaryColor),
                            ),
                            numeric: true),
                        DataColumn(
                            label: Text('V',
                                style:
                                    TextStyle(color: AppColors.secundaryColor)),
                            numeric: true),
                        DataColumn(
                            label: Text('D',
                                style:
                                    TextStyle(color: AppColors.secundaryColor)),
                            numeric: true),
                        DataColumn(
                            label: Text('E',
                                style:
                                    TextStyle(color: AppColors.secundaryColor)),
                            numeric: true),
                        DataColumn(
                            label: Text('GP-GC / SG',
                                style:
                                    TextStyle(color: AppColors.secundaryColor)),
                            numeric: true),
                        DataColumn(
                            label: Text('P',
                                style:
                                    TextStyle(color: AppColors.secundaryColor)),
                            numeric: true),
                        DataColumn(
                            label: Text('AP',
                                style:
                                    TextStyle(color: AppColors.secundaryColor)),
                            numeric: true),
                      ],
                      rows: controller.auth.rankings
                          .map((e) => DataRow(cells: [
                                DataCell(Text(
                                  e.name,
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                                DataCell(Text(
                                  '${e.j}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                                DataCell(Text(
                                  '${e.v}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                                DataCell(Text(
                                  '${e.d}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                                DataCell(Text(
                                  '${e.e}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                                DataCell(Row(
                                  children: [
                                    Text(
                                      '(${e.gp} - ${e.gc})',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: 10,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.secundaryColor,
                                      ),
                                    ),
                                    const SizedBox(width: 5),
                                    Badge(
                                      backgroundColor:
                                          e.sg > 0 ? Colors.green : Colors.red,
                                      label: Text('${e.sg}'),
                                    )
                                  ],
                                )),
                                /*  DataCell(Text(
                                  '${e.gc}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )), */
                                /* DataCell(Text(
                                  '${e.sg}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )), */
                                DataCell(Text(
                                  '${e.p}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                                DataCell(Text(
                                  '${e.ap}',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 11,
                                    fontWeight: FontWeight.normal,
                                    color: AppColors.secundaryColor,
                                  ),
                                )),
                              ]))
                          .toList(),
                    ),
                  )
                ],
              ),
            );
          },
        ));
  }
}
