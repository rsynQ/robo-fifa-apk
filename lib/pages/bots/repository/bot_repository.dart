import '../../../core/constants/endpoints.dart';
import '../../../core/helpers/secure_storage.dart';
import '../../../core/http/http_manager.dart';
import '../../../core/result/data_result.dart';
import '../../../core/result/data_string_result.dart';
import '../../../models/bot/bot.dart';
import '../../../models/reports/report_model.dart';

class BotRepository {
  final HttpManager _http = HttpManager();

  Future<DataStringResult> createBot(Bot bot) async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    final response = await _http.restRequest(
      url: Endpoints.createBot,
      method: HttpMethods.post,
      body: bot.toMap(),
      headers: {'Authorization': 'Bearer $token'},
    );

    if (response['result'] != null) {
      final data = response['result'];
      return DataStringResult.success(data);
    } else {
      return DataStringResult.error(response['error']);
    }
  }

  Future<DataStringResult> deleteBot(Bot bot) async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    final response = await _http.restRequest(
      url: Endpoints.deleteBot,
      method: HttpMethods.post,
      body: bot.toMap(),
      headers: {'Authorization': 'Bearer $token'},
    );

    if (response['result'] != null) {
      final data = response['result'];
      return DataStringResult.success(data);
    } else {
      return DataStringResult.error(response['error']);
    }
  }

  Future<DataResult<Bot>> getBots() async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    final response = await _http.restRequest(
      url: Endpoints.bots,
      method: HttpMethods.get,
      headers: {'Authorization': 'Bearer $token'},
    );

    if (response['result'] != null) {
      List<Bot> data = (List<Map<String, dynamic>>.from(response['result']))
          .map(Bot.fromMap)
          .toList();

      return DataResult.success(data);
    } else {
      return DataResult.error(response['error']);
    }
  }

  Future<DataResult<ReportModel>> getBotDetails(int botID) async {
    final token = await SecureStorage.getLocalData(key: 'access_token');
    final response = await _http.restRequest(
      url: Endpoints.botDetails,
      method: HttpMethods.get,
      body: {'bot_id': botID},
      headers: {'Authorization': 'Bearer $token'},
    );

    if (response['result'] != null) {
      List<ReportModel> data =
          (List<Map<String, dynamic>>.from(response['result']))
              .map(ReportModel.fromMap)
              .toList();

      return DataResult.success(data);
    } else {
      return DataResult.error(response['error']);
    }
  }
}
