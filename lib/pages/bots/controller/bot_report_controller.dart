import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/models/reports/report_model.dart';

import '../../../models/bot/bot.dart';
import '../../../models/chart/pie_data.dart';
import '../repository/bot_repository.dart';

class BotReportController extends GetxController {
  final repository = BotRepository();
  final List<ReportModel> _reports = [];
  late Bot bot;

  List<ReportModel> get reports => _reports;
  Map<String, List<ReportModel>> groupedByHour = {};
  List<PieData> chartByHour = [];

  @override
  void onInit() async {
    bot = Get.arguments;
    await getDetails();
    super.onInit();
  }

  void groupedList() {
    for (var report in reports) {
      DateTime dateTime = DateTime.parse(report.created_at);
      int offsetInHours = DateTime.now().timeZoneOffset.inHours;
      DateTime localDateTime = dateTime.add(Duration(hours: offsetInHours));

      String hour = '${localDateTime.hour.toString().padLeft(2, '0')}:00';
      if (!groupedByHour.containsKey(hour)) {
        groupedByHour[hour] = [];
      }
      groupedByHour[hour]!.add(report);
    }

    List<String> sortedHours = groupedByHour.keys.toList()..sort();

    for (var hour in sortedHours) {
      num totalRoi = 0;
      for (var report in groupedByHour[hour]!) {
        totalRoi += report.roi;
      }
      chartByHour.add(PieData(xData: hour, yData: totalRoi));
    }
  }

  Future<void> getDetails() async {
    EasyLoading.show();
    final result = await repository.getBotDetails(bot.id!);
    result.when(
      success: (data) {
        _reports.assignAll(data);
        groupedList();
        EasyLoading.dismiss();
        update();
      },
      error: (message) {
        EasyLoading.showError(message);
      },
    );
  }
}
