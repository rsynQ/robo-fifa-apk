import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/auth/controller/auth_controller.dart';

import '../../../models/bot/bot.dart';
import '../../../models/bot/condition_model.dart';
import '../repository/bot_repository.dart';

class BotController extends GetxController {
  final auth = Get.find<AuthController>();
  final List<Bot> _bots = [];
  final List<ConditionModel> _conditions = [];
  final List<String> _players = [];
  final repository = BotRepository();

  int typeList = 0;
  List<ConditionModel> get conditions => _conditions;
  List<String> get player => _players;
  List<Bot> get bots => _bots;

  @override
  void onInit() async {
    await getBots();
    super.onInit();
  }

  Future<void> createBot(Bot bot, [bool param = true]) async {
    EasyLoading.show();
    final result = await repository.createBot(bot);
    result.when(
      success: (data) {
        EasyLoading.dismiss();
        getBots();
      },
      error: (message) {
        EasyLoading.showError(message);
      },
    );
    if (param) {
      Get.back(result: true);
    }
  }

  Future<void> deleteBot(Bot bot) async {
    EasyLoading.show();
    final result = await repository.deleteBot(bot);
    result.when(
      success: (data) {
        EasyLoading.dismiss();
        getBots();
      },
      error: (message) {
        EasyLoading.showError(message);
      },
    );
    Get.back(result: true);
  }

  Future<void> getBots() async {
    EasyLoading.show();
    final result = await repository.getBots();
    result.when(
      success: (data) {
        _bots.assignAll(data);
        EasyLoading.dismiss();
        update();
      },
      error: (message) {
        EasyLoading.showError(message);
      },
    );
  }

  void addPlayers(List<String>? data) {
    _players.assignAll((data ?? []));
    update();
  }

  void addTypePlayer(int data) {
    typeList = data;
    update();
  }

  void addCondition(ConditionModel condition) {
    _conditions.add(condition);
    update();
  }

  void removeCondition(int index) {
    if (index >= 0 && index < _conditions.length) {
      _conditions.removeAt(index);
      update();
    }
  }

  Map<int, String> treatConditions() {
    String last;
    String player;
    return _conditions.asMap().map((index, condition) {
      if (condition.consider == 0) {
        last = 'Últ. 20 jogos';
      } else if (condition.consider == 1) {
        last = 'Últ. 10 jogos';
      } else if (condition.consider == 2) {
        last = 'Últ. 5 jogos';
      } else {
        last = 'Últ. 2 jogos';
      }

      if (condition.player == 0) {
        player = 'do Player A';
      } else if (condition.player == 1) {
        player = 'do Player B';
      } else if (condition.player == 3) {
        player = 'do Player A com Equipe atual ';
      } else if (condition.player == 4) {
        player = 'do Player B com Equipe atual ';
      } else {
        player = 'de Ambos';
      }

      if (condition.type == 0) {
        return MapEntry(index,
            'Média de Gols: ${condition.condition == 0 ? 'Maior ou igual' : 'Menor ou igual'} a média ${condition.type_avg == 0 ? 'nos Confrontos Direto ' : 'Individual'} $player de ${condition.average} gols no ${condition.modality == 0 ? 'FT' : 'HT'} dos $last');
      } else if (condition.type == 1) {
        return MapEntry(index,
            'Média de Vitórias: ${condition.condition == 0 ? 'Maior ou igual' : 'Menor ou igual'} a média de vitorias ${condition.type_avg == 0 ? 'nos Confrontos Direto ' : 'Individual'} $player de ${condition.average}%  no ${condition.modality == 0 ? 'FT' : 'HT'} dos $last');
      } else if (condition.type == 2) {
        return MapEntry(index,
            'Estatística de Handicap:  ${condition.condition == 0 ? 'Maior ou igual' : 'Menor ou igual'} ${condition.type_avg == 0 ? 'nos Confrontos Direto ' : 'Individual'} $player a ${condition.average}%  no ${condition.modality == 0 ? 'FT' : 'HT'} com diferença de ${condition.difference} Gols dos $last');
      }

      return MapEntry(index, '');
    });
  }

  Future<void> activeBot(Bot bot, int active) async {
    bot.active = active;
    await createBot(bot, false);
  }
}
