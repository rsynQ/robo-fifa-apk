import 'package:get/get.dart';
import 'package:robo_fifa/pages/bots/controller/bot_report_controller.dart';

class BotReportBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(BotReportController());
  }
}
