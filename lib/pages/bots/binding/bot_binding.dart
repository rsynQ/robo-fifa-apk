import 'package:get/get.dart';
import 'package:robo_fifa/pages/bots/controller/bot_controller.dart';

class BotBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(BotController());
  }
}
