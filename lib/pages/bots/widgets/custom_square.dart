import 'package:flutter/material.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';

class CustomSquare extends StatelessWidget {
  final String name;
  final IconData icon;
  final void Function()? onTap;
  const CustomSquare(
      {super.key, required this.name, required this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 200,
        height: 200,
        decoration: BoxDecoration(
          color: AppColors.primaryColor,
          border: Border.all(
            color: AppColors.secundaryColor,
            width: .5,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 48,
              color: AppColors.textColor,
            ),
            const SizedBox(height: 20),
            Text(
              name,
              style: TextStyle(
                fontFamily: 'Roboto',
                color: AppColors.secundaryColor,
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
