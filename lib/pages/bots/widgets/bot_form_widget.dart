// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:robo_fifa/core/ui/forms/custom_dropdown_button_form.dart';
import 'package:robo_fifa/core/ui/forms/custom_text_form_field.dart';
import 'package:robo_fifa/pages/bots/controller/bot_controller.dart';

import '../../../models/bot/condition_model.dart';

class BotFormWidget extends StatefulWidget {
  final String title;
  final int? type;
  const BotFormWidget({super.key, required this.title, this.type = 0});

  @override
  State<BotFormWidget> createState() => _BotFormWidgetState();
}

class _BotFormWidgetState extends State<BotFormWidget> {
  final controller = Get.find<BotController>();
  int typeAvg = 0;
  final _formKey = GlobalKey<FormState>();
  ConditionModel condition = ConditionModel();
  late MaskTextInputFormatter maskDecimal;
  @override
  Widget build(BuildContext context) {
    if (widget.type == 0) {
      maskDecimal = MaskTextInputFormatter(
          mask: '#.##',
          filter: {"#": RegExp(r'[0-9]')},
          type: MaskAutoCompletionType.lazy);
    } else {
      maskDecimal = MaskTextInputFormatter(
          mask: '###',
          filter: {"#": RegExp(r'[0-9]')},
          type: MaskAutoCompletionType.lazy);
    }

    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.title,
              style: const TextStyle(
                  fontFamily: 'Roboto', color: Colors.white, fontSize: 20),
            ),
            CustomDropdownButtonForm(
              value: 0,
              label: 'Modalidade',
              onChanged: (p0) {},
              onSaved: (p0) => condition.modality = p0!,
              items: [
                const DropdownMenuItem(value: 0, child: Text('Partida')),
                if (widget.type != 1)
                  const DropdownMenuItem(value: 1, child: Text('Intervalo')),
              ],
            ),
            Visibility(
              visible: widget.type == 2,
              child: CustomDropdownButtonForm(
                value: 1,
                label: 'Diferença de Gols',
                onChanged: (p0) {},
                onSaved: (p0) => condition.difference = p0!,
                items: const [
                  DropdownMenuItem(value: 1, child: Text('1 Gol')),
                  DropdownMenuItem(value: 2, child: Text('2 Gols')),
                ],
              ),
            ),
            CustomDropdownButtonForm(
              value: 0,
              label: 'Tipo',
              onChanged: (p0) {
                if (p0 != null) {
                  setState(() {
                    typeAvg = p0;
                  });
                }
              },
              onSaved: (p0) => condition.type_avg = p0!,
              items: const [
                DropdownMenuItem(
                    value: 0, child: Text('Médias do Confrontos Direto')),
                DropdownMenuItem(value: 1, child: Text('Médias Individuais')),
              ],
            ),
            CustomDropdownButtonForm(
              value: widget.type != 1 ? 2 : 0,
              label: 'Jogador',
              onChanged: (p0) {},
              onSaved: (p0) => condition.player = p0!,
              items: [
                if (widget.type != 2)
                  const DropdownMenuItem(value: 0, child: Text('Player A')),
                if (widget.type != 2)
                  const DropdownMenuItem(value: 1, child: Text('Player B')),
                if (widget.type == 1)
                  const DropdownMenuItem(
                      value: 3, child: Text('Player A + Team A')),
                if (widget.type == 1)
                  const DropdownMenuItem(
                      value: 4, child: Text('Player B + Team B')),
                if (widget.type != 1)
                  const DropdownMenuItem(value: 2, child: Text('Ambos')),
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 4,
                  child: CustomDropdownButtonForm(
                    value: 0,
                    label: 'Condição',
                    onChanged: (p0) {},
                    onSaved: (p0) => condition.condition = p0!,
                    items: const [
                      DropdownMenuItem(value: 0, child: Text('Maior ou Igual')),
                      DropdownMenuItem(value: 1, child: Text('Menor ou Igual')),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: CustomTextFormField(
                    height: 45,
                    initialValue: null,
                    label: 'Média',
                    onSaved: (p0) =>
                        condition.average = double.tryParse(p0!) ?? 0,
                    keyboardType: TextInputType.number,
                    inputFormatters: [maskDecimal],
                    validator: (p0) {
                      if (p0 == null) {
                        return '--';
                      }

                      if (p0.isEmpty) {
                        return '--';
                      }

                      return null;
                    },
                  ),
                )
              ],
            ),
            CustomDropdownButtonForm(
              value: 0,
              label: 'Considerar',
              onChanged: (p0) {},
              onSaved: (p0) => condition.consider = p0!,
              items: const [
                DropdownMenuItem(value: 0, child: Text('Últimos 20 jogos')),
                DropdownMenuItem(value: 1, child: Text('Últimos 10 jogos')),
                DropdownMenuItem(value: 2, child: Text('Últimos 5 jogos')),
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: TextButton(
                      onPressed: () => Get.back(),
                      child: const Text(
                        'Fechar',
                        style:
                            TextStyle(fontFamily: 'Roboto', color: Colors.grey),
                      )),
                ),
                Expanded(
                  flex: 5,
                  child: ElevatedButton(
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      if (_formKey.currentState!.validate()) {
                        _formKey.currentState!.save();
                        condition.type = widget.type;
                        controller.addCondition(condition);
                        Get.back();
                      }
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0xFF46C48F)),
                    child: const Text(
                      'Adicionar',
                      style:
                          TextStyle(fontFamily: 'Roboto', color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
