import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';

import '../../../core/ui/forms/custom_dropdown_button_form.dart';
import '../../../core/ui/forms/custom_multi_select_form.dart';
import '../controller/bot_controller.dart';

class BotFormPlayersWidget extends GetView<BotController> {
  final String title;
  const BotFormPlayersWidget({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: const TextStyle(
                  fontFamily: 'Roboto', color: Colors.white, fontSize: 20),
            ),
            CustomDropdownButtonForm(
              onChanged: (p0) {
                controller.addTypePlayer((p0 ?? 0));
              },
              value: controller.typeList,
              label: 'Condição',
              items: const [
                DropdownMenuItem(
                    value: 0,
                    child: Text('Permitir apenas jogos com os jogadores')),
                DropdownMenuItem(
                    value: 1,
                    child: Text('Não permitir jogos com os jogadores'))
              ],
            ),
            CustomMultiSelectForm(
              label: 'Jogadores',
              initialValue: controller.player,
              items: controller.auth.players
                  .map((e) => MultiSelectItem(e.player_name, e.player_name))
                  .toList(),
              onConfirm: (p0) {
                controller.addPlayers(p0);
              },
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: TextButton(
                      onPressed: () => Get.back(),
                      child: const Text(
                        'Fechar',
                        style:
                            TextStyle(fontFamily: 'Roboto', color: Colors.grey),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
