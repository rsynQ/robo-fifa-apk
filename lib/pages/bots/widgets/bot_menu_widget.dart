import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/ui/app_colors.dart';
import 'package:robo_fifa/pages/bots/widgets/bot_form_widget.dart';

import 'bot_form_players_widget.dart';
import 'custom_square.dart';

class BotMenuWidget extends StatelessWidget {
  const BotMenuWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomSquare(
              name: 'Média de Gols',
              icon: Icons.balance,
              onTap: () => Get.dialog(
                barrierDismissible: false,
                barrierColor: AppColors.primaryColor,
                const BotFormWidget(title: 'Média de Gols'),
              ),
            ),
            CustomSquare(
              name: 'Média de Vitórias',
              icon: Icons.balance,
              onTap: () => Get.dialog(
                barrierDismissible: false,
                barrierColor: AppColors.primaryColor,
                const BotFormWidget(title: 'Média de Vitórias', type: 1),
              ),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomSquare(
              name: 'Estatística de Handicap',
              icon: Icons.equalizer,
              onTap: () => Get.dialog(
                barrierDismissible: false,
                barrierColor: AppColors.primaryColor,
                const BotFormWidget(title: 'Estatística de Handicap', type: 2),
              ),
            ),
            CustomSquare(
              name: 'BlackList/WhiteList',
              icon: Icons.article,
              onTap: () => Get.dialog(
                barrierDismissible: false,
                barrierColor: AppColors.primaryColor,
                const BotFormPlayersWidget(title: 'BlackList/WhiteList'),
              ),
            ),
          ],
        )
      ],
    );
  }
}
