import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/components/custom_app_bar.dart';
import 'package:robo_fifa/pages/bots/controller/bot_report_controller.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../core/ui/app_colors.dart';
import '../../../models/chart/pie_data.dart';

class BotReportsScreen extends StatelessWidget {
  const BotReportsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const CustomAppBar(),
        body: GetBuilder<BotReportController>(
          builder: (controller) {
            return Column(
              children: [
                Text(
                  '${controller.bot.bot_name}',
                  style: const TextStyle(
                      fontFamily: 'Roboto', fontWeight: FontWeight.bold),
                ),
                Card(
                  surfaceTintColor: AppColors.primaryColor,
                  color: AppColors.primaryColor,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        const SizedBox(width: 15),
                        Icon(
                          Icons.send,
                          size: 18,
                          color: AppColors.secundaryColor.withOpacity(.8),
                        ),
                        Text(
                          'TIPS ENVIADAS: ${(controller.bot.qt ?? 0)}',
                          style: TextStyle(
                            color: AppColors.secundaryColor.withOpacity(.8),
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Icon(
                          Icons.percent,
                          size: 18,
                          color: AppColors.secundaryColor.withOpacity(.8),
                        ),
                        Text(
                          'ODD MÉD. ${(controller.bot.odd_med ?? 0).toStringAsFixed(2)}',
                          style: TextStyle(
                            color: AppColors.secundaryColor.withOpacity(.8),
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Icon(
                          Icons.attach_money,
                          size: 18,
                          color: (controller.bot.roi ?? 0) >= 0
                              ? AppColors.textColor.withOpacity(.8)
                              : Colors.red.withOpacity(.8),
                        ),
                        Text(
                          'ROI: ${(controller.bot.roi ?? 0).toStringAsFixed(2)}',
                          style: TextStyle(
                            color: (controller.bot.roi ?? 0) >= 0
                                ? AppColors.textColor.withOpacity(.8)
                                : Colors.red.withOpacity(.8),
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  surfaceTintColor: AppColors.primaryColor,
                  color: AppColors.primaryColor,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: SfCartesianChart(
                    title: ChartTitle(
                        text: 'ROI POR HORÁRIO',
                        textStyle: TextStyle(
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                            color: AppColors.secundaryColor)),
                    primaryXAxis: CategoryAxis(
                      majorGridLines: const MajorGridLines(width: 0),
                      minorGridLines: const MinorGridLines(width: 0),
                      labelStyle: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 10),
                    ),
                    primaryYAxis: NumericAxis(
                      labelStyle: const TextStyle(
                        color: Colors.white,
                        fontFamily: 'Roboto',
                      ),
                    ),
                    series: <ChartSeries>[
                      ColumnSeries<PieData, String>(
                        name: 'ROI',
                        dataSource: controller.chartByHour,
                        xValueMapper: (PieData report, _) => report.xData,
                        yValueMapper: (PieData report, _) => report.yData,
                        pointColorMapper: (PieData report, _) =>
                            report.yData > 0 ? Colors.green : Colors.red,
                      )
                    ],
                    tooltipBehavior: TooltipBehavior(enable: true),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: controller.reports.length,
                    itemBuilder: (context, index) {
                      final report = controller.reports[index];
                      return Card(
                        surfaceTintColor: AppColors.primaryColor,
                        color: AppColors.primaryColor,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    '${report.home_name} (${report.home_team})',
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: AppColors.secundaryColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500),
                                  ),
                                  const Spacer(),
                                  Text(
                                    'Odd: ${(report.line_odd).toStringAsFixed(2)}  Linha: ${report.handicap}',
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: AppColors.secundaryColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    '${report.away_name} (${report.away_team})',
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: AppColors.secundaryColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500),
                                  ),
                                  const Spacer(),
                                  Badge(
                                    largeSize: 20.0,
                                    backgroundColor: report.roi > 0
                                        ? Colors.green
                                        : Colors.red,
                                    label: Text(
                                        'ROI: ${(report.roi).toStringAsFixed(2)}'),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            );
          },
        ));
  }
}
