import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:robo_fifa/core/helpers/market_helder.dart';
import 'package:robo_fifa/pages/bots/controller/bot_controller.dart';
import 'package:robo_fifa/pages/bots/widgets/bot_menu_widget.dart';
import 'package:robo_fifa/routes/pages_route.dart';

import '../../../core/components/custom_app_bar.dart';
import '../../../core/ui/app_colors.dart';
import '../../../core/ui/forms/custom_dropdown_button_form.dart';
import '../../../core/ui/forms/custom_multi_select_form.dart';
import '../../../core/ui/forms/custom_text_form_field.dart';
import '../../../models/bot/bot.dart';

class BotCreateScreen extends StatefulWidget {
  const BotCreateScreen({super.key});

  @override
  State<BotCreateScreen> createState() => _BotCreateScreenState();
}

class _BotCreateScreenState extends State<BotCreateScreen> {
  final controller = Get.find<BotController>();
  final _formKey = GlobalKey<FormState>();
  late Bot bot;

  @override
  void initState() {
    bot = Get.arguments;
    controller.conditions.assignAll((bot.conditions ?? []));
    controller.typeList = (bot.bw_type ?? 0);
    controller.player.assignAll((bot.bwlist ?? []));
    bot.market ??= 0;
    print(bot.market);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var maskDecimal = MaskTextInputFormatter(
        mask: '#.##',
        filter: {"#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy);

    var maskMoneyDecimal = MaskTextInputFormatter(
        mask: '###.##',
        filter: {"#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy);

    return Scaffold(
        appBar: const CustomAppBar(),
        floatingActionButton: bot.id != null
            ? SpeedDial(
                iconTheme: const IconThemeData(color: Colors.white),
                animatedIcon: AnimatedIcons.add_event,
                animatedIconTheme:
                    const IconThemeData(size: 34.0, color: Colors.white),
                backgroundColor: AppColors.primaryColor,
                children: [
                  SpeedDialChild(
                    child: const Icon(
                      Icons.delete,
                    ),
                    labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                    label: 'Deletar BOT',
                    onTap: () async {
                      bool confirmLogout = await Get.dialog(
                        AlertDialog(
                          alignment: Alignment.center,
                          backgroundColor: AppColors.primaryColor,
                          title: Text('CONFIRMAÇÃO',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: AppColors.textColor,
                                  fontFamily: 'Roboto',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold)),
                          content: Text(
                              'Tem certeza de que deseja remover ${bot.bot_name}?',
                              style: TextStyle(
                                color: AppColors.secundaryColor,
                                fontFamily: 'Roboto',
                                fontSize: 14,
                              )),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Get.back(result: false),
                              child: Text('CANCELAR',
                                  style: TextStyle(
                                    color: AppColors.secundaryColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                  )),
                            ),
                            TextButton(
                              onPressed: () => Get.back(result: true),
                              child: Text('SIM, DESEJO REMOVER',
                                  style: TextStyle(
                                    color: AppColors.textColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                  )),
                            ),
                          ],
                        ),
                      );

                      if (confirmLogout == true) {
                        controller.deleteBot(bot);
                      }
                    },
                  ),
                  SpeedDialChild(
                    child: const Icon(
                      Icons.copy,
                    ),
                    labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                    label: 'Duplicar',
                    onTap: () async {
                      bool confirmLogout = await Get.dialog(
                        AlertDialog(
                          alignment: Alignment.center,
                          backgroundColor: AppColors.primaryColor,
                          title: Text('CONFIRMAÇÃO',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: AppColors.textColor,
                                  fontFamily: 'Roboto',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold)),
                          content: Text('Deseja duplicar o ${bot.bot_name}?',
                              style: TextStyle(
                                color: AppColors.secundaryColor,
                                fontFamily: 'Roboto',
                                fontSize: 14,
                              )),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Get.back(result: false),
                              child: Text('CANCELAR',
                                  style: TextStyle(
                                    color: AppColors.secundaryColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                  )),
                            ),
                            TextButton(
                              onPressed: () => Get.back(result: true),
                              child: Text('SIM, DESEJO DUPLICAR',
                                  style: TextStyle(
                                    color: AppColors.textColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                  )),
                            ),
                          ],
                        ),
                      );

                      if (confirmLogout == true) {
                        bot.active = 0;
                        bot.id = null;
                        controller.createBot(bot);
                      }
                    },
                  ),
                  SpeedDialChild(
                    child: const Icon(Icons.equalizer_rounded),
                    labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                    label: 'Ver Entradas',
                    onTap: () =>
                        Get.toNamed(PagesRoute.botReportRoute, arguments: bot),
                  ),
                ],
              )
            : null,
        body: SingleChildScrollView(
          child: GetBuilder<BotController>(
            builder: (controller) {
              return Form(
                key: _formKey,
                child: Column(
                  children: [
                    Card(
                      surfaceTintColor: AppColors.primaryColor,
                      color: AppColors.greyColor,
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: ExpansionTile(
                        initiallyExpanded: (bot.id) == null ? true : false,
                        maintainState: true,
                        iconColor: AppColors.textColor,
                        collapsedIconColor: AppColors.textColor,
                        leading: Icon(
                          Icons.my_library_books_outlined,
                          color: AppColors.secundaryColor,
                        ),
                        title: Text(
                          'Escolha o Mercado',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.bold,
                              color: AppColors.secundaryColor),
                        ),
                        children: [
                          CustomTextFormField(
                            initialValue: bot.bot_name,
                            label: 'NOME PARA O BOOT',
                            onSaved: (p0) => bot.bot_name = p0!,
                            validator: (p0) {
                              if (p0 == null) {
                                return 'Campo obrigátorio';
                              }

                              if (p0.length < 2) {
                                return 'Precisa pelo menos 3 caracteres';
                              }

                              return null;
                            },
                          ),
                          CustomDropdownButtonForm(
                            value: (bot.market ?? 0),
                            label: 'Mercado',
                            items: const [
                              DropdownMenuItem(
                                  value: 0, child: Text('Resultado Final')),
                              DropdownMenuItem(
                                  value: 2, child: Text('Handicap Asiático')),
                              DropdownMenuItem(
                                  value: 3, child: Text('+/- Gols na Partida')),
                            ],
                            onChanged: (p0) {
                              setState(() {
                                bot.market = p0!;
                              });
                            },
                            onSaved: (p0) => bot.market = p0!,
                          ),
                          Visibility(
                            visible: (bot.market ?? 0) != 3,
                            child: CustomDropdownButtonForm(
                              onSaved: (value) => bot.field_command = value,
                              onChanged: (value) {
                                setState(() {
                                  bot.field_command = value!;
                                });
                              },
                              value: (bot.field_command ?? 0),
                              label: 'Player',
                              items: const [
                                DropdownMenuItem(
                                  value: 0,
                                  child: Text(
                                    'Casa',
                                  ),
                                ),
                                DropdownMenuItem(
                                  value: 1,
                                  child: Text(
                                    'Fora',
                                  ),
                                ),
                                DropdownMenuItem(
                                  value: 2,
                                  child: Text(
                                    'Favorito',
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: (bot.market ?? 0) == 3,
                            child: CustomDropdownButtonForm(
                              value: (bot.internal_line ?? 0),
                              label: ' +/- ',
                              items: const [
                                DropdownMenuItem(value: 0, child: Text('Over')),
                                DropdownMenuItem(
                                    value: 1, child: Text('Under')),
                              ],
                              onChanged: (p0) {},
                              onSaved: (p0) => bot.internal_line = p0!,
                            ),
                          ),
                          Visibility(
                            visible: (bot.market ?? 0) > 1,
                            child: CustomMultiSelectForm(
                              label: 'Linha do Mercado',
                              initialValue: (bot.market_line ?? []),
                              items: bot.market == 3
                                  ? MarketHelder.goalsMarket()
                                      .map((e) => MultiSelectItem(e, e))
                                      .toList()
                                  : MarketHelder.handicapMarket()
                                      .map((e) => MultiSelectItem(e, e))
                                      .toList(),
                              onConfirm: (p0) {},
                              onSaved: (p0) => bot.market_line = p0!,
                            ),
                          ),
                          Visibility(
                            visible: !((bot.field_command ?? -1) == 2 &&
                                bot.market == 0),
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                children: [
                                  Container(
                                    decoration: const BoxDecoration(
                                      color: Color(0xFF46C48F),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          bottomLeft: Radius.circular(10)),
                                    ),
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      'ODD ',
                                      style: TextStyle(
                                        fontFamily: 'Roboto',
                                        color: AppColors.textColorWhite,
                                        fontSize: 14.0,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: CustomTextFormField(
                                      controller: null,
                                      initialValue: bot.odd == null
                                          ? ''
                                          : bot.odd.toString(),
                                      label: 'Limitar Odds',
                                      onSaved: (p0) => bot.odd = num.parse(p0!),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [maskDecimal],
                                      padding: const EdgeInsets.all(0),
                                      validator:
                                          ((bot.field_command ?? -1) == 2 &&
                                                  bot.market == 1)
                                              ? null
                                              : (p0) {
                                                  if (p0 == null) {
                                                    return 'Obrigatório';
                                                  }

                                                  if (p0.isEmpty) {
                                                    return 'Obrigatório';
                                                  }

                                                  return null;
                                                },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                    color: Color(0xFF46C48F),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        bottomLeft: Radius.circular(10)),
                                  ),
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    'STAKE',
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      color: AppColors.textColorWhite,
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: CustomTextFormField(
                                    controller: null,
                                    initialValue: bot.stake == null
                                        ? ''
                                        : bot.stake.toString(),
                                    label: 'Qual a stake ?',
                                    onSaved: (p0) => bot.stake = num.parse(p0!),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [maskMoneyDecimal],
                                    padding: const EdgeInsets.all(0),
                                    validator: (p0) {
                                      if (p0 == null) {
                                        return 'Obrigatório';
                                      }

                                      if (p0.isEmpty) {
                                        return 'Obrigatório';
                                      }

                                      return null;
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                TextButton(
                                    onPressed: () => Get.back(),
                                    child: Text('CANCELAR',
                                        style: TextStyle(
                                          color: AppColors.secundaryColor,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.bold,
                                        ))),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: AppColors.primaryColor),
                                  onPressed: controller.auth.isPRO()
                                      ? () {
                                          if (_formKey.currentState!
                                              .validate()) {
                                            _formKey.currentState!.save();
                                            bot.conditions =
                                                controller.conditions;
                                            bot.bwlist = controller.player;
                                            bot.bw_type = controller.typeList;

                                            if ((bot.field_command ?? -1) ==
                                                    2 &&
                                                bot.market == 1) {
                                              bot.odd = 0;
                                            }

                                            controller.createBot(bot);
                                          }
                                        }
                                      : () => Get.toNamed(PagesRoute.plans),
                                  child: Text(
                                    bot.id != null ? 'SALVAR' : 'CRIAR BOT',
                                    style: TextStyle(
                                      color: AppColors.textColorWhite,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      surfaceTintColor: AppColors.primaryColor,
                      color: AppColors.greyColor,
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: ExpansionTile(
                        maintainState: true,
                        iconColor: AppColors.textColor,
                        collapsedIconColor: AppColors.textColor,
                        leading: Icon(
                          Icons.track_changes,
                          color: AppColors.secundaryColor,
                        ),
                        title: Text(
                          'Condições no Ao Vivo',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.bold,
                              color: AppColors.secundaryColor),
                        ),
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                    color: Color(0xFF46C48F),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        bottomLeft: Radius.circular(10)),
                                  ),
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    'TEMPO',
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      color: AppColors.textColorWhite,
                                      fontSize: 13.0,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: CustomTextFormField(
                                    initialValue: bot.minute == null
                                        ? ''
                                        : bot.minute.toString(),
                                    onSaved: (p0) =>
                                        bot.minute = int.parse(p0!),
                                    height: 43,
                                    label: 'Maior ou igual',
                                    keyboardType: TextInputType.number,
                                    padding: const EdgeInsets.all(0),
                                    validator: (p0) {
                                      if (p0 == null) {
                                        return 'Obrigatório';
                                      }

                                      if (p0.isEmpty) {
                                        return 'Obrigatório';
                                      }

                                      return null;
                                    },
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Text(
                                  'minutos de jogo',
                                  style: TextStyle(
                                    color: AppColors.secundaryColor,
                                    fontFamily: 'Roboto',
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                          CustomMultiSelectForm(
                            onSaved: (p0) => bot.leaderboards = p0,
                            label: 'Placares',
                            initialValue: (bot.leaderboards ?? []),
                            items: MarketHelder.leaderboards()
                                .map((e) => MultiSelectItem(e, e))
                                .toList(),
                            onConfirm: (p0) {},
                          ),
                        ],
                      ),
                    ),
                    Card(
                      surfaceTintColor: AppColors.primaryColor,
                      color: AppColors.greyColor,
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: ExpansionTile(
                        maintainState: true,
                        iconColor: AppColors.textColor,
                        collapsedIconColor: AppColors.textColor,
                        leading: Icon(
                          Icons.equalizer,
                          color: AppColors.secundaryColor,
                        ),
                        title: Text(
                          'Condições de Análise',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.bold,
                              color: AppColors.secundaryColor),
                        ),
                        children: [
                          TextButton.icon(
                              onPressed: () =>
                                  Get.dialog(const BotMenuWidget()),
                              icon: const Icon(Icons.add_circle,
                                  color: Color(0xFF46C48F)),
                              label: const Text('Adicionar Condição',
                                  style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFF46C48F)))),
                          ...controller
                              .treatConditions()
                              .entries
                              .map((entry) => ListTile(
                                    shape: Border(
                                        bottom: BorderSide(
                                            color: AppColors.secundaryColor)),
                                    leading: Icon(Icons.balance,
                                        color: AppColors.secundaryColor),
                                    trailing: IconButton(
                                      onPressed: () =>
                                          controller.removeCondition(entry.key),
                                      icon: const Icon(
                                        Icons.remove_circle,
                                        color: Colors.red,
                                      ),
                                    ),
                                    title: Padding(
                                      padding: const EdgeInsets.all(10),
                                      child: Text(
                                        entry.value,
                                        style: TextStyle(
                                          color: AppColors.textColorWhite,
                                          fontFamily: 'Roboto',
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ))
                              .toList(),
                          Visibility(
                            visible: controller.player.isNotEmpty,
                            child: Text(
                              controller.typeList == 0
                                  ? 'Permitir apenas jogos com players abaixo:'
                                  : 'Não permitir jogos com players abaixo:',
                              style: TextStyle(
                                color: AppColors.textColor,
                                fontFamily: 'Roboto',
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          ...controller.player.map(
                            (e) => ListTile(
                              shape: Border(
                                  bottom: BorderSide(
                                      color: AppColors.secundaryColor)),
                              leading: Icon(Icons.person,
                                  color: AppColors.secundaryColor),
                              subtitle: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Text(
                                  e,
                                  style: TextStyle(
                                    color: AppColors.textColorWhite,
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ));
  }
}
