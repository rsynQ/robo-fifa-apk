import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/ui/dialog/info_dialog.dart';
import 'package:robo_fifa/pages/bots/controller/bot_controller.dart';
import 'package:robo_fifa/routes/pages_route.dart';

import '../../../core/components/custom_app_bar.dart';
import '../../../core/ui/app_colors.dart';
import '../../../models/bot/bot.dart';

class BotScreen extends StatelessWidget {
  const BotScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const CustomAppBar(),
        body: GetBuilder<BotController>(
          builder: (controller) {
            return Column(
              children: [
                Card(
                  surfaceTintColor: AppColors.primaryColor,
                  color: AppColors.primaryColor,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(width: 15),
                          Icon(
                            Icons.send,
                            size: 18,
                            color: AppColors.secundaryColor.withOpacity(.8),
                          ),
                          const SizedBox(width: 5),
                          Text(
                            'TIPS ENVIDAS: ${controller.bots.fold<num>(0, (sum, t) => sum + (t.qt ?? 0))}',
                            style: TextStyle(
                              color: AppColors.secundaryColor.withOpacity(.8),
                              fontFamily: 'Roboto',
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const Spacer(),
                          IconButton(
                            onPressed: () => Get.dialog(InfoDialog(
                              title: 'IMPORTANTE',
                              children: [
                                Row(
                                  children: [
                                    Icon(Icons.power_settings_new,
                                        color: AppColors.greyColor),
                                    const SizedBox(width: 10),
                                    Text(
                                      'BOT está desativado',
                                      style: TextStyle(
                                        color: AppColors.secundaryColor
                                            .withOpacity(.8),
                                        fontFamily: 'Roboto',
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(height: 10),
                                Row(
                                  children: [
                                    Icon(Icons.power_settings_new,
                                        color: AppColors.textColor),
                                    const SizedBox(width: 10),
                                    Text(
                                      'BOT está ativado',
                                      style: TextStyle(
                                        color: AppColors.secundaryColor
                                            .withOpacity(.8),
                                        fontFamily: 'Roboto',
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(height: 20),
                                Text(
                                  '(Clique no icone para ativar/desativar)',
                                  style: TextStyle(
                                    color: AppColors.secundaryColor
                                        .withOpacity(.8),
                                    fontFamily: 'Roboto',
                                    fontStyle: FontStyle.italic,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )),
                            icon: const Icon(Icons.info),
                            color: AppColors.secundaryColor,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(width: 15),
                          Icon(
                            Icons.percent,
                            size: 18,
                            color: AppColors.secundaryColor.withOpacity(.8),
                          ),
                          const SizedBox(width: 2),
                          Text(
                            'ODD MÉDIA: ${(controller.bots.fold<num>(0, (sum, o) => sum + (o.odd_med ?? 0)) / 3).toStringAsFixed(2)}',
                            style: TextStyle(
                              color: AppColors.secundaryColor.withOpacity(.8),
                              fontFamily: 'Roboto',
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(width: 15),
                          Icon(
                            Icons.attach_money,
                            size: 18,
                            color: AppColors.secundaryColor.withOpacity(.8),
                          ),
                          Text(
                            'ROI: ${controller.bots.fold<num>(0, (sum, r) => sum + (r.roi ?? 0)).toStringAsFixed(2)}',
                            style: TextStyle(
                              color: AppColors.secundaryColor.withOpacity(.8),
                              fontFamily: 'Roboto',
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const Spacer(),
                          TextButton.icon(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: AppColors.primaryColor),
                              onPressed: () => Get.toNamed(
                                  PagesRoute.botCreateRoute,
                                  arguments: Bot()),
                              icon: Icon(Icons.add_circle,
                                  color: AppColors.textColor),
                              label: Text(
                                'CRIAR BOT',
                                style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.textColor),
                              )),
                          const SizedBox(width: 10)
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: controller.bots.length,
                    itemBuilder: (context, index) {
                      final bot = controller.bots[index];
                      return Card(
                        surfaceTintColor: AppColors.primaryColor,
                        color: AppColors.primaryColor,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: ListTile(
                          onTap: () => Get.toNamed(PagesRoute.botCreateRoute,
                              arguments: bot),
                          leading: IconButton(
                            onPressed: () => (bot.active ?? 0) == 0
                                ? controller.activeBot(bot, 1)
                                : controller.activeBot(bot, 0),
                            icon: const Icon(Icons.power_settings_new),
                            color: (bot.active ?? 0) == 1
                                ? AppColors.textColor
                                : AppColors.greyColor,
                            iconSize: 30,
                          ),
                          title: Text(
                            '${(bot.bot_name?.length ?? 0) <= 27 ? bot.bot_name : '${bot.bot_name?.substring(0, 27)}...'}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: AppColors.secundaryColor,
                                fontFamily: 'Roboto',
                                fontSize: 12),
                            overflow: TextOverflow.ellipsis,
                          ),
                          subtitle: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColors.textColor.withOpacity(.1),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const SizedBox(width: 15),
                                  Icon(
                                    Icons.send,
                                    size: 18,
                                    color: AppColors.secundaryColor
                                        .withOpacity(.8),
                                  ),
                                  const SizedBox(width: 5),
                                  Text(
                                    'TIPS: ${(bot.qt ?? 0)}',
                                    style: TextStyle(
                                      color: AppColors.secundaryColor
                                          .withOpacity(.8),
                                      fontFamily: 'Roboto',
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  Icon(
                                    Icons.percent,
                                    size: 18,
                                    color: AppColors.secundaryColor
                                        .withOpacity(.8),
                                  ),
                                  const SizedBox(width: 2),
                                  Text(
                                    'ODD ${(bot.odd_med ?? 0).toStringAsFixed(2)}',
                                    style: TextStyle(
                                      color: AppColors.secundaryColor
                                          .withOpacity(.8),
                                      fontFamily: 'Roboto',
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  Icon(
                                    Icons.attach_money,
                                    size: 18,
                                    color: (bot.roi ?? 0) >= 0
                                        ? AppColors.textColor.withOpacity(.8)
                                        : Colors.red.withOpacity(.8),
                                  ),
                                  Text(
                                    'ROI: ${(bot.roi ?? 0).toStringAsFixed(2)}',
                                    style: TextStyle(
                                      color: (bot.roi ?? 0) >= 0
                                          ? AppColors.textColor.withOpacity(.8)
                                          : Colors.red.withOpacity(.8),
                                      fontFamily: 'Roboto',
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            );
          },
        ));
  }
}
