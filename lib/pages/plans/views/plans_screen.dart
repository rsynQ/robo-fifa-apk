import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:robo_fifa/core/helpers/date_helper.dart';
import 'package:robo_fifa/core/helpers/launch_helper.dart';
import 'package:robo_fifa/pages/plans/controller/plans_controller.dart';

import '../../../core/components/custom_app_bar.dart';
import '../../../core/ui/app_colors.dart';

class PlansScreen extends GetView<PlansController> {
  const PlansScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Visibility(
            visible: controller.auth.isPRO(),
            child: Card(
              surfaceTintColor: AppColors.primaryColor,
              color: AppColors.primaryColor,
              elevation: 4,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'VOCÊ AGORA É UM PRO',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: AppColors.textColorWhite,
                      ),
                    ),
                    const Divider(),
                    Text(
                      'Vencimento: ${DateHelper.formatDateTime((controller.auth.user?.transaction_date ?? 1))}',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.textColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Card(
            surfaceTintColor: AppColors.primaryColor,
            color: AppColors.primaryColor,
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Text('PRO',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 20,
                          color: AppColors.textColor)),
                  Text('R\$ 27,99/mês',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 20,
                          color: AppColors.textColorWhite)),
                  const Divider(color: Colors.white),
                  const SizedBox(height: 20),
                  Text('Crie até 5 Bots',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 14,
                        color: AppColors.textColorWhite,
                      )),
                  const SizedBox(height: 10),
                  Text('Navegue pela aplicação sem anúncios',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const SizedBox(height: 10),
                  Text('Suporte: Segunda a Sábado',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const Divider(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.textColor),
                    onPressed: () async => await LaunchHelper.getLaunchUrl(
                        Uri.parse('https://t.me/suporte24h_app')),
                    child: const Text('Dúvidas',
                        style: TextStyle(color: Colors.black)),
                  )
                ],
              ),
            ),
          ),
          Card(
            surfaceTintColor: AppColors.primaryColor,
            color: AppColors.primaryColor,
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'PRO',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 20,
                            color: AppColors.textColor),
                      ),
                      const Badge(label: Text('-5%')),
                    ],
                  ),
                  Text('R\$ 79,77/trimestral',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 20,
                          color: AppColors.textColorWhite)),
                  const Divider(color: Colors.white),
                  const SizedBox(height: 20),
                  Text('Crie até 5 Bots',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const SizedBox(height: 10),
                  Text('Navegue pela aplicação sem anúncios',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const SizedBox(height: 10),
                  Text('Suporte: Segunda a Sábado',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const Divider(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.textColor),
                    onPressed: () async => await LaunchHelper.getLaunchUrl(
                        Uri.parse('https://t.me/suporte24h_app')),
                    child: const Text('Dúvidas',
                        style: TextStyle(color: Colors.black)),
                  )
                ],
              ),
            ),
          ),
          Card(
            surfaceTintColor: AppColors.primaryColor,
            color: AppColors.primaryColor,
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'PRO',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 20,
                            color: AppColors.textColor),
                      ),
                      const Badge(label: Text('-20%')),
                    ],
                  ),
                  Text('R\$ 268,70/Anual',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 20,
                          color: AppColors.textColorWhite)),
                  const Divider(color: Colors.white),
                  const SizedBox(height: 20),
                  Text('Crie até 5 Bots',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const SizedBox(height: 10),
                  Text('Navegue pela aplicação sem anúncios',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const SizedBox(height: 10),
                  Text('Suporte: Segunda a Sábado',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: AppColors.textColorWhite)),
                  const Divider(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.textColor),
                    onPressed: () async => await LaunchHelper.getLaunchUrl(
                        Uri.parse('https://t.me/suporte24h_app')),
                    child: const Text('Dúvidas',
                        style: TextStyle(color: Colors.black)),
                  )
                ],
              ),
            ),
          )
        ],
      )),
    );
  }
}
