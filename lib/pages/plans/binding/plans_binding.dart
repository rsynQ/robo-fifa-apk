import 'package:get/get.dart';

import '../controller/plans_controller.dart';

class PlansBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(PlansController());
  }
}
