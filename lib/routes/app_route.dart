import 'package:get/get.dart';
import 'package:robo_fifa/pages/plans/binding/plans_binding.dart';
import 'package:robo_fifa/pages/plans/views/plans_screen.dart';

import '../pages/alerts/binding/alert_binding.dart';
import '../pages/alerts/views/alert_screen.dart';
import '../pages/analyzer/binding/analyzer_binding.dart';
import '../pages/analyzer/views/analyzer_screen.dart';
import '../pages/auth/views/sign_in_screen.dart';
import '../pages/auth/views/sign_up_screen.dart';
import '../pages/bots/binding/bot_binding.dart';
import '../pages/bots/binding/bot_report_binding.dart';
import '../pages/bots/views/bot_create_screen.dart';
import '../pages/bots/views/bot_reports_screen.dart';
import '../pages/bots/views/bot_screen.dart';
import '../pages/splash/splash_screen.dart';
import '../pages/welcome/binding/welcome_binding.dart';
import '../pages/welcome/views/welcome_screen.dart';
import 'pages_route.dart';

class AppRoute {
  static final pages = <GetPage>[
    GetPage(name: PagesRoute.splashRoute, page: () => const SplashScreen()),
    GetPage(name: PagesRoute.signInRoute, page: () => const SignInScreen()),
    GetPage(name: PagesRoute.signUpRoute, page: () => const SignUpScreen()),
    GetPage(
        name: PagesRoute.welcomeRoute,
        page: () => const WelcomeScreen(),
        binding: WelcomeBinding()),
    GetPage(
        name: PagesRoute.botRoute,
        page: () => const BotScreen(),
        binding: BotBinding()),
    GetPage(
      name: PagesRoute.botCreateRoute,
      page: () => const BotCreateScreen(),
    ),
    GetPage(
        name: PagesRoute.botReportRoute,
        page: () => const BotReportsScreen(),
        binding: BotReportBinding()),
    GetPage(
        name: PagesRoute.alertRoute,
        page: () => const AlertScreen(),
        binding: AlertBinding()),
    GetPage(
        name: PagesRoute.analyzerRoute,
        page: () => const AnalyzerScreen(),
        binding: AnalyzerBinding()),
    GetPage(
        name: PagesRoute.plans,
        page: () => const PlansScreen(),
        binding: PlansBinding()),
  ];
}
