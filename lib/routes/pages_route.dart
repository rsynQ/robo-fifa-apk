class PagesRoute {
  static const String splashRoute = '/';
  static const String signUpRoute = '/signUp';
  static const String signInRoute = '/signIn';
  static const String welcomeRoute = '/welcome';
  static const String analyzerRoute = '/analyzer';
  static const String plans = '/plans';
  static const String botRoute = '/bots';
  static const String alertRoute = '/alerts';
  static const String botCreateRoute = '/bot-create';
  static const String botReportRoute = '/bot-report';
  static const String noInternetRoute = '/no-internet';
}
